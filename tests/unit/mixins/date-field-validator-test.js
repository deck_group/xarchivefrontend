import EmberObject from '@ember/object';
import DateFieldValidatorMixin from 'frontend/mixins/date-field-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | date-field-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let DateFieldValidatorObject = EmberObject.extend(DateFieldValidatorMixin);
    let subject = DateFieldValidatorObject.create();
    assert.ok(subject);
  });
});
