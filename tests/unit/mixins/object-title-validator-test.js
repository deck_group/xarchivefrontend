import EmberObject from '@ember/object';
import ObjectTitleValidatorMixin from 'frontend/mixins/object-title-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-title-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectTitleValidatorObject = EmberObject.extend(ObjectTitleValidatorMixin);
    let subject = ObjectTitleValidatorObject.create();
    assert.ok(subject);
  });
});
