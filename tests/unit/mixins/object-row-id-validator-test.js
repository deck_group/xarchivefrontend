import EmberObject from '@ember/object';
import ObjectRowIdValidatorMixin from 'frontend/mixins/object-row-id-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-row-id-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectRowIdValidatorObject = EmberObject.extend(ObjectRowIdValidatorMixin);
    let subject = ObjectRowIdValidatorObject.create();
    assert.ok(subject);
  });
});
