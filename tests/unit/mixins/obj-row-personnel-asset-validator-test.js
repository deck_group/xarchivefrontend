import EmberObject from '@ember/object';
import ObjRowPersonnelAssetValidatorMixin from 'frontend/mixins/obj-row-personnel-asset-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | obj-row-personnel-asset-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjRowPersonnelAssetValidatorObject = EmberObject.extend(ObjRowPersonnelAssetValidatorMixin);
    let subject = ObjRowPersonnelAssetValidatorObject.create();
    assert.ok(subject);
  });
});
