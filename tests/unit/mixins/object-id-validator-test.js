import EmberObject from '@ember/object';
import ObjectIdValidatorMixin from 'frontend/mixins/object-id-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-id-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectIdValidatorObject = EmberObject.extend(ObjectIdValidatorMixin);
    let subject = ObjectIdValidatorObject.create();
    assert.ok(subject);
  });
});
