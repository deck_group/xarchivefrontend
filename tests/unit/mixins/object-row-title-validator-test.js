import EmberObject from '@ember/object';
import ObjectRowTitleValidatorMixin from 'frontend/mixins/object-row-title-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-row-title-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectRowTitleValidatorObject = EmberObject.extend(ObjectRowTitleValidatorMixin);
    let subject = ObjectRowTitleValidatorObject.create();
    assert.ok(subject);
  });
});
