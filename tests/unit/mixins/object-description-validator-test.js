import EmberObject from '@ember/object';
import ObjectDescriptionValidatorMixin from 'frontend/mixins/object-description-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-description-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectDescriptionValidatorObject = EmberObject.extend(ObjectDescriptionValidatorMixin);
    let subject = ObjectDescriptionValidatorObject.create();
    assert.ok(subject);
  });
});
