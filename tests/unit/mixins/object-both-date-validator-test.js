import EmberObject from '@ember/object';
import ObjectBothDateValidatorMixin from 'frontend/mixins/object-both-date-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | object-both-date-validator', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ObjectBothDateValidatorObject = EmberObject.extend(ObjectBothDateValidatorMixin);
    let subject = ObjectBothDateValidatorObject.create();
    assert.ok(subject);
  });
});
