import EmberObject from '@ember/object';
import NumberFieldMixin from 'frontend/mixins/number-field';
import { module, test } from 'qunit';

module('Unit | Mixin | number-field', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let NumberFieldObject = EmberObject.extend(NumberFieldMixin);
    let subject = NumberFieldObject.create();
    assert.ok(subject);
  });
});
