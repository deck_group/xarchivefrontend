import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | asset-view', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:asset-view');
    assert.ok(controller);
  });
});
