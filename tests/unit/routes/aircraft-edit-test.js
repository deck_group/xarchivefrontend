import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | aircraft-edit', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:aircraft-edit');
    assert.ok(route);
  });
});
