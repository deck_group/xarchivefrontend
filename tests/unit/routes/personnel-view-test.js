import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | personnel-view', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:personnel-view');
    assert.ok(route);
  });
});
