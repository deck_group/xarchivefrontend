import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | aircraft-show', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:aircraft-show');
    assert.ok(route);
  });
});
