import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | asset-view', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:asset-view');
    assert.ok(route);
  });
});
