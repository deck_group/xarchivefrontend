import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    title: attr('string'),
    taken:attr('string'),
    assetid:attr('string'),
    directory:attr('string'),
    imageurls:attr('string'),
});
