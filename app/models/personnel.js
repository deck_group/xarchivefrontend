import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    bdno: attr('string'),
    name: attr('string'),
    currentrank:attr('string'),
    isretired:attr('string'),
    imagedataarray:attr(),
    rankhistory:attr('array'),
    imageurls:attr('array'),
    status:attr('string'),
    nameplate:attr('string'),
});

