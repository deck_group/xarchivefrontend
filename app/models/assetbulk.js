import DS from 'ember-data';
const {
  Model,
  attr
} = DS;

export default Model.extend({
    assetid: attr('string'),
    title: attr('string'),
    description:attr('string'),
    taken:attr('string'),
    personnel:attr('array'),
    event: attr('array'),
    location:attr('array'),
    aircraft: attr('array'),
    imagedataarray:attr('string'),
    imagename:attr('string'),
    imageurls:attr('string'),
    status:attr('string'),
    galurl:attr('string'),
    secrecylevel:attr('string'),
    studiophotography:attr('string'),
    othertags:attr('array'),
    comment:attr('array'),
    caption:attr('string'),
    directory:attr('string'),
    originalimageurl:attr('string'),
    oriimagedataarray:attr('string'),
    isrecognised:attr('string'),
    originalimagename:attr('string'),
    isSearch:attr('string'),

});
