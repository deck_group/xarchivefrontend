import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    name: attr('string'),
    code: attr('string'),
    rankno:attr('number'),
    description: attr('string'),
    orderid:attr('number'),
});

