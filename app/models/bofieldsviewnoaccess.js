import DS from 'ember-data';
const {
  Model,
  attr
} = DS;

export default Model.extend({
    user: attr('string'),
    role: attr('string'),
    businessobject: attr('string'),
    attribute: attr('string'),
});
