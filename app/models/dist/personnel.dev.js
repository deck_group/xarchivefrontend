"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  bdno: attr('string'),
  name: attr('string'),
  currentrank: attr('string'),
  isretired: attr('string'),
  imagedataarray: attr(),
  rankhistory: attr('array'),
  imageurls: attr('array'),
  status: attr('string'),
  nameplate: attr('string')
});

exports["default"] = _default;