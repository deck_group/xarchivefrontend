"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

var _Model$extend;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend((_Model$extend = {
  assetid: attr('string'),
  title: attr('string'),
  taken: attr('string'),
  personnel: attr('array'),
  event: attr('array'),
  location: attr('array'),
  aircraft: attr('array'),
  imagedataarray: attr('string'),
  imagename: attr('string')
}, _defineProperty(_Model$extend, "imagename", attr('string')), _defineProperty(_Model$extend, "imageurls", attr()), _defineProperty(_Model$extend, "status", attr('string')), _Model$extend));

exports["default"] = _default;