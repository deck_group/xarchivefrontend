import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    directory: attr('string'),
    imageurls: attr('string'),
    title: attr('string'),
    assetid: attr('string'),
    taken: attr('string'),
});

