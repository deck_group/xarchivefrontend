import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    bdno: attr('string'),
    imagename: attr('string'),
    imagedirectory: attr('string'),
});

