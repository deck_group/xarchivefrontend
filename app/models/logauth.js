import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    loginid: attr('string'),
    password: attr('string'),
    token: attr('string'),
    role:attr('string'),
});
