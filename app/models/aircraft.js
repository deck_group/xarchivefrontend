import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    aircraftid: attr('string'),
    name: attr('string'),
    imagedataarray:attr('array'),
    imageurls:attr('array'),
    description: attr('string'),
    status:attr('string'),
});

