import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    name: attr('string'),
    x: attr('number'),
    y: attr('number'),
    height: attr('number'),
    width: attr('number'),
    imagedata: attr('string'),
});

