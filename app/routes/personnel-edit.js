import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            personnel: store.find('personnel', params.personnel_id),
            nextactions: store.query('nextallowableaction', {
                objectname: 'personnel,rank,event,location',
                startstatus: 'draft',
                endstatus: 'approved,end,declined',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'personnel',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),

            noteditable:store.query('bofieldseditnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'personnel'
            }),
            role: localStorage.getItem('role')

        })
    },

    setupController(controller, model) {
        controller.set('personnel', model.personnel);
        controller.set('nextactions', model.nextactions);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('role', model.role);
        controller.set('noteditables', model.noteditable);
        this._super(controller, model);
    }
});
