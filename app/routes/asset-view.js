import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Route.extend({
    urls: service("service"),
    store: service(),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    async model(params) {
        const store = this.get('store');
        var hurl = this.get("urls.url") + '/gallery'
        var gallery = await $.ajax({
            method: "GET",
            url: hurl
        }).then(bind(this, (data) => {
            return data["result"]
        }))
        return hash({
            asset: store.find('asset', params.asset_id),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'asset',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),
            notviewable: store.query('bofieldsviewnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'asset'
            }),
            directory: gallery
        })
    },
    setupController(controller, model) {
        controller.set('asset', model.asset);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('notviewables', model.notviewable);
        controller.set('directory', model.directory);
        
        this._super(controller, model);
    }
});
