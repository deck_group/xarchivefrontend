import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            aircraft: store.find('aircraft', params.aircraft_id),
            nextactions: store.query('nextallowableaction', {
                objectname: 'aircraft,rank,event,location',
                startstatus: 'draft',
                endstatus: 'approved,end,declined',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'aircraft',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),

    
            role: localStorage.getItem('role')

        })
    },

    setupController(controller, model) {
        controller.set('aircraft', model.aircraft);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('role', model.role);
        this._super(controller, model);
    }
});
