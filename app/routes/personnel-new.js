import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    urls: service("service"),
    store: service(),
    init() {
        this._super(...arguments)
        if (localStorage.getItem("token") === "undefined") {
            window.location = this.get("urls.signout");
        }
    },

    model() {
        const store = this.get('store');
        return hash({
            nextactions: store.query('nextallowableaction', {
                objectname: 'personnel,rank',
                startstatus: 'start',
                endstatus: 'approved,end',
                role: localStorage.getItem('role')
            })
        })
    },

    setupController(controller, model) {
        controller.set('nextactions', model.nextactions);
        this._super(controller, model);
    }
});
