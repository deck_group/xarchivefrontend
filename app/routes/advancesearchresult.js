import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';


export default Route.extend({
    urls: service("service"),
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },

    async model(params){
        var hurl=this.get("urls.url")+localStorage.getItem("url")
        var searches = await $.ajax({
            method: "GET",
            url: hurl
        }).then(bind(this, (data) => {
            console.log(data)
            return data
        }))
        return hash({
            searches: searches
        })

    },

    setupController(controller, model) {
        controller.set('searches', model.searches);
        this._super(controller, model);
    }



});
