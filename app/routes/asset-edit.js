import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';


export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    async model(params) {
        const store = this.get('store');
        var hurl = this.get("urls.url") + '/gallery'
        var gallery = await $.ajax({
            method: "GET",
            url: hurl
        }).then(bind(this, (data) => {
            return data["result"]
        }))
        return hash({
            asset: store.find('asset', params.asset_id),
            nextactions: store.query('nextallowableaction', {
                objectname: 'asset,rank,event,location',
                startstatus: 'draft',
                endstatus: 'approved,end,declined',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'asset',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),

            noteditable:store.query('bofieldseditnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'asset'
            }),
            role: localStorage.getItem('role'),
            asset_id:params.asset_id,
            directory: gallery

        })
    },

    setupController(controller, model) {
        controller.set('asset', model.asset);
        controller.set('nextactions', model.nextactions);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('role', model.role);
        controller.set('noteditables', model.noteditable);
        controller.set('asset_id',model.asset_id);
        controller.set('directory', model.directory);
        this._super(controller, model);
    }
});
