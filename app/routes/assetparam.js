import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Route.extend({
    store: service(),
    urls: service("service"),
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    async model(params) {
        const store = this.get('store');
        var hurl = this.get("urls.url") + '/assets'
        var hurl2 = this.get("urls.url") + '/gallery'
        var gallery = await $.ajax({
            method: "GET",
            url: hurl2
        }).then(bind(this, (data) => {
            return data["result"]
        }))

        return hash({
            nextactions: store.query('nextallowableaction', {
                objectname: 'asset',
                startstatus: 'start',
                endstatus: 'draft',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'asset',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),
            notviewable: store.query('bofieldsviewnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'asset'
            }),
            hurl: hurl,
            directory: gallery,
            assets: store.query('asset', {
                directory:params.filter,
                page: {
                    number: params.page,
                    size: params.size

                }
            })

        })
    },

    setupController(controller, model) {
        controller.set('directory', model.directory);
        controller.set('nextactions', model.nextactions);
        controller.set('params', model.params);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('notviewables', model.notviewable);
        controller.set('counts', model.assets.count);
        controller.set('assets', model.assets);
        controller.set('hurl', model.hurl);
        this._super(controller, model);
    }
});

