"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _service = require("@ember/service");

var _rsvp = require("rsvp");

var _jquery = _interopRequireDefault(require("jquery"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  urls: (0, _service.inject)("service"),
  store: (0, _service.inject)(),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem("token") === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model() {
    var store, hurl, gallery;
    return regeneratorRuntime.async(function model$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            store = this.get('store');
            hurl = this.get("urls.url") + '/gallery';
            _context.next = 4;
            return regeneratorRuntime.awrap(_jquery["default"].ajax({
              method: "GET",
              url: hurl
            }).then((0, _runloop.bind)(this, function (data) {
              return data["result"];
            })));

          case 4:
            gallery = _context.sent;
            return _context.abrupt("return", (0, _rsvp.hash)({
              nextactions: store.query('nextallowableaction', {
                objectname: 'asset,rank,event,location',
                startstatus: 'start',
                endstatus: 'approved,end',
                role: localStorage.getItem('role')
              }),
              asset: store.findAll('asset').then(function (assets) {
                return assets.sortBy('id').reverse();
              }),
              directory: gallery
            }));

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, null, this);
  },
  setupController: function setupController(controller, model) {
    controller.set('nextactions', model.nextactions);
    controller.set('assets', model.asset);
    controller.set('directory', model.directory);

    this._super(controller, model);
  }
});

exports["default"] = _default;