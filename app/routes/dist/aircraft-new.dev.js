"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _service = require("@ember/service");

var _rsvp = require("rsvp");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  urls: (0, _service.inject)("service"),
  store: (0, _service.inject)(),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem("token") === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model() {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      nextactions: store.query('nextallowableaction', {
        objectname: 'aircraft',
        startstatus: 'start',
        endstatus: 'approved,end',
        role: localStorage.getItem('role')
      }),
      aircraft: store.findAll("aircraft").then(function (aircrafts) {
        return aircrafts.sortBy('id').reverse();
      })
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('nextactions', model.nextactions);
    controller.set('aircraft', model.aircraft);

    this._super(controller, model);
  }
});

exports["default"] = _default;