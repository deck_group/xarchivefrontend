"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _service = require("@ember/service");

var _rsvp = require("rsvp");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  storage: (0, _service.inject)("storage"),
  model: function model() {
    return (0, _rsvp.hash)({
      image: this.get("storage").imagestorage,
      oriimage: this.get("storage").imagestorage,
      originalheight: this.get("storage").height,
      originalwidth: this.get("storage").width,
      isInverted: this.get("storage").isInverted,
      brigthness: this.get("storage").brigthness,
      contrast: this.get("storage").contrast,
      hue: this.get("storage").hue,
      saturation: this.get("storage").saturation,
      lighness: this.get("storage").lighness
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('image', model.image);
    controller.set('oriimage', model.oriimage);
    controller.set('originalheight', model.originalheight);
    controller.set('originalwidth', model.originalwidth);
    controller.set('isInverted', model.isInverted);
    controller.set('brigthness', model.brigthness);
    controller.set('contrast', model.contrast);
    controller.set('hue', model.hue);
    controller.set('saturation', model.saturation);
    controller.set('lightness', model.lighness);

    this._super(controller, model);
  }
});

exports["default"] = _default;