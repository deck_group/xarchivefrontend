import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';


export default Route.extend({
    urls: service("service"),
    store: service(),
    init() {
        this._super(...arguments)
        if (localStorage.getItem("token") === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    async model() {
        const store = this.get('store');
        var hurl = this.get("urls.url") + '/gallery'
        var gallery = await $.ajax({
            method: "GET",
            url: hurl
        }).then(bind(this, (data) => {
            return data["result"]
        }))
        return hash({
            directory: gallery,
            hurl:hurl
        })
    },

    setupController(controller, model) {
        console.log(model.directory)
        controller.set('directory', model.directory);
        controller.set('hurl', model.hurl);
        this._super(controller, model);
    }

});
