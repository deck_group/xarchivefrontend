import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';


export default Route.extend({
    urls: service("service"),
    store: service(),
    init() {
        this._super(...arguments)
        if (localStorage.getItem("token") === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    async model() {
        const store = this.get('store');
        var hurl = this.get("urls.url") + '/gallery'
        var gallery = await $.ajax({
            method: "GET",
            url: hurl
        }).then(bind(this, (data) => {
            return data["result"]
        }))
        var hurl2 = this.get("urls.url") + '/galleries'
        var galleries = await $.ajax({
            method: "GET",
            url: hurl2
        }).then(bind(this, (data) => {
            return data['data']
        }))

        return hash({
            directory: gallery,
            galleries:galleries,
            hurl:hurl2,
        })
    },
    setupController(controller, model) {
        controller.set('directory', model.directory);
        controller.set('galleries', model.galleries.result);
        controller.set('counts', model.galleries.count);
        controller.set('next_url', model.galleries.next_url);
        controller.set('previous_url', model.galleries.previous_url);
        controller.set('hurl', model.hurl);
        this._super(controller, model);
    }
});
