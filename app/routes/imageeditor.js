import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    storage: service("storage"),
    model() {
        return hash({
            image: this.get("storage").imagestorage,
            oriimage: this.get("storage").imagestorage,
            originalheight: this.get("storage").height,
            originalwidth: this.get("storage").width,
            isInverted: this.get("storage").isInverted,
            brigthness: this.get("storage").brigthness,
            contrast: this.get("storage").contrast,
            hue: this.get("storage").hue,
            saturation:this.get("storage").saturation,
            lighness:this.get("storage").lighness,

        })
    },

    setupController(controller, model) {
        controller.set('image', model.image);
        controller.set('oriimage', model.oriimage);
        controller.set('originalheight', model.originalheight);
        controller.set('originalwidth', model.originalwidth);
        controller.set('isInverted', model.isInverted);
        controller.set('brigthness', model.brigthness);
        controller.set('contrast', model.contrast);
        controller.set('hue', model.hue);
        controller.set('saturation', model.saturation);
        controller.set('lightness', model.lighness);
        this._super(controller, model);
    }



});
