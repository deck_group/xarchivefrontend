import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model() {
        const store = this.get('store');
        return hash({
            nextactions: store.query('nextallowableaction', {
                objectname: 'aircraft',
                startstatus: 'start',
                endstatus: 'draft',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'aircraft',
                status: 'draft,approved,declined',
                role: localStorage.getItem('role')
            }),
            notviewable: store.query('bofieldsviewnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'aircraft'
            }),
            aircraft: store.findAll('aircraft'),
        })
    },

    setupController(controller, model) {
        controller.set('nextactions', model.nextactions);
        controller.set('aircrafts', model.aircraft);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('notviewables', model.notviewable);
        this._super(controller, model);
    }
});
