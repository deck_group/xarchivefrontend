import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    urls: service("service"),
    store: service(),
    init() {
        this._super(...arguments)
        if (localStorage.getItem("token") === "undefined") {
            window.location = this.get("urls.signout");
        }
    },

    model() {
        const store = this.get('store');
        return hash({
            nextactions: store.query('nextallowableaction', {
                objectname: 'aircraft',
                startstatus: 'start',
                endstatus: 'approved,end',
                role: localStorage.getItem('role')
            }),
            aircraftid:Math.random().toString(36).slice(2),
            
        })
    },

    setupController(controller, model) {
        controller.set('nextactions', model.nextactions);
        controller.set('aircraftid', model.aircraftid);
        this._super(controller, model);
    }
});
