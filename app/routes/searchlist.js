import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    urls: service("service"),
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
   model(params){
        const store = this.get('store');
        var keyword=``
        localStorage.setItem("search",undefined)
        var hurl=``
        if(localStorage.getItem("advance") === "true"){
            localStorage.setItem("advance","false")
            hurl = `advancesearch`
            keyword=localStorage.getItem("url")

        }
        else{
            keyword=localStorage.getItem("search")
            hurl = `search`
        }
        return hash({
            searches:store.query(hurl, {
                filter:keyword,
                page: {
                    number: params.page,
                    size: params.size

                }
            }),
            data:keyword
        })
    },

    setupController(controller, model) {
        controller.set('searches', model.searches);
        controller.set('data', model.data);
        this._super(controller, model);
    }
});
