import Component from '@ember/component';
import dateFieldValidator from '../mixins/date-field-validator';

export default Component.extend(dateFieldValidator,{
    tagName: 'tr',
    isReadonly: false,
    fieldtype: 'date',
    label: "Object Title",
    tableLabelClass: "col-sm-4 padding-0 o_td_label",
    labelClass: "o_form_label",
    message: undefined,
    tdClass: "col-sm-7 padding-left",
    isPresence: true,
    before:new Date(),
    flagerror:false,
    after:'01/01/1900',
    inputClass: "form-control required",
    actions: {
        setDate() {
            this.set('message', undefined)
            this.set('message', this.validate(this.get('dt'),this.get('isPresence'),this.get('label'),this.get('after'),this.get('before')))
            if(this.get('message').length>0){
                this.set('flagerror',true)
            }
            if(this.get('flagerror')===false){
                // this.attrs.changeDate(this.get('dt'), this)
            }
            
        }
    }
});
