import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    showErrors: true,
    tagName: 'tr',
    isReadonly: false,
    message: undefined,
    flagerror: false,
    format: "^[a-zA-Z0-9\\s.@-_']*$",
    isPresence: true,
    leasdingspace: true,
    label: "Object Title",
    tableLabelClass: "col-sm-4 padding-0 o_td_label",
    labelClass: "o_form_label",
    tdClass: "col-sm-7 padding-left",
    inputClass: "cursor-slider",
    minlength: -100,
    maxlength: 100,
    isBoardcast: false,
    holderflag: undefined,
    type: ``,
    step:1,
    bus: service("pubsub"),
    data: 0,
    mode: ``,
    actions: {
        setValue(data) {
            if (this.get("isBoardcast") === true) {
                if (this.get("holderflag") !== undefined) {
                    this.get('bus').sendImageCorrectionOption(data, this.get("holderflag"));
                }

            }
        }
    }
});
