import Component from '@ember/component';
import {
    inject as service
} from '@ember/service';


export default Component.extend({
    store: service(),
    newmodel: {},
    actions: {
        save(newmodel) {
            var msg = " - "
            if (Object.keys(newmodel).length === 0) {
                msg = msg + "Please Provide name"
                this.send("errormsgdef", msg)

            } else {
                let newlocation = this.get("store").createRecord("location");
                newlocation.set("name", newmodel.name)
                newlocation.set("address", newmodel.address)
                newlocation.set("lat", 0.00)
                newlocation.set("lng", 0.00)
                if (newlocation.get('name') === undefined) {
                    msg = msg + "Please Provide name"
                    this.send("errormsgdef", msg)
                }
                else {
                    if (newlocation.get("address") === undefined) {
                        newlocation.set("address", "N/A")
                    }

                    newlocation.save().then(() => {
                        this.set('newmodel', {})
                        var ok = document.getElementById('discardLocation');
                        ok.addEventListener("click", this.send("modalclose"));

                    })


                }
            }
        },
        modalclose() {
            var modal = document.getElementById('locationNew');
            this.get('tasksController').send("callLocation")
            modal.style.display = 'none'
        },
        errormsgdef(msg) {
            this.get('flashMessages').danger(msg, {
                timeout: 3000,
                priority: 100,
                sticky: false,
                showProgress: true
            })
        }
    }
});
