import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `td`,
    classNames: [`col-sm-1`],
    totalarray: [],
    keyAccess: `sn`,
    needtobroadcast: false,
    bus: service("pubsub"),
    actions: {
        delete(event) {
            var newarray = []
            console.log(event)
            console.log(event.parentView.model)
            console.log(event.parentView.model[this.get('keyAccess')])
            var keytoremove = event.parentView.model[this.get('keyAccess')]
            var oldArray = this.get('totalarray')
           
            for (var i = 0; i < oldArray.length; i++) {
                if (oldArray[i][this.get('keyAccess')] !== keytoremove) {
                    newarray.push(oldArray[i])
                }
            }
            this.set('totalarray', newarray)
            if (this.get('needtobroadcast') === true) {
                this.get('bus').revisedPublish(newarray);
            } else {
                var element_delete = document.getElementById(event.parentView.elementId);
                element_delete.remove();
            }
        }

    }
});
