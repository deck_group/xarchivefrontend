import Component from '@ember/component';
import objectRowID from '../mixins/object-row-id-validator';

export default Component.extend(objectRowID,{
    tagName:`td`,
    classNames:[`col-sm-1`],
    classInput:`form-control input-txt-table`,
    isReadonly:false
});
