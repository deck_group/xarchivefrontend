import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `td`,
    classNames: [`col-sm-2`],
    classInput: `form-control required-3`,
    isReadonly: false,
    placeholder: ``,
    store: service(),
    keyaccess: `name`,
    bus: service("pubsub"),
    keystosearch: [],
    needtobroadcast: false,
    newdat: undefined,
    sortitem: `name`,
    data: undefined,
    orderby: `asc`,
    model: computed(function () {
        this.set('newdat', this.get("store").findAll(this.get('modelname')))
        return this.get("store").findAll(this.get('modelname'))
    }),

    sortingByArray: computed(function () {
        var array = []
        var data = this.get("sortitem") + ':' + this.get("orderby")
        array.push(data)
        return array
    }),
    sortedCollection: computed.sort('model', 'sortingByArray'),

    actions: {
        setvalue() {
            var that = this
            if (this.get("needtobroadcast") === true) {
                if (this.get("newdat").arrangedContent.length < 1) { 

                }
                else {
                    this.get("newdat").forEach(function (item) {
                        if (item[that.get('keyaccess')] === that.get("data")) {
                            that.get('bus').dataPublish(item, that.get('sn'));
                        }
                    })
                }

            }

        }
    }
});
