import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    classNames: [`row maringT`],
    classRow: `col-sm-12`,
    jstreeActionReceiver: undefined,
    jstreeSelectedNodes: ``,
    bus: service("pubsub"),
    actions: {
        selectionNode(data) {
            this.get('jstreeActionReceiver').send('getPath', data)
        },
        handleJstreePathNode(node) {
            var dir = `/`
            for (var i = 0; i < node.length; i++) {
                dir = dir + node[i] + `/`
            }
            this.get('bus').selectedDirectoryPub(dir);
        },
    }

});
