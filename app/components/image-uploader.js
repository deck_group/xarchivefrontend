import Component from '@ember/component';

export default Component.extend({
    tagName: `div`,
    classRow: `row`,
    src: `/images/placeimage.png`,
    classTdOne: `col-sm-4 col-xs-12 col-12`,
    classTdTwo: `col-sm-8 col-xs-12 col-12`,
    classImage: `col-sm-12 col-xs-12 col-12 asset-image-preview`,
    classNames: [`col-sm-12 col-xs-12 col-12`],
    classInput: `col-sm-12 col-xs-12 col-12 form-control`,
    classInputRequires: `form-control required`,
    classTable:`table-new`,
    tableid:`table_gal`,
    tbodyid:`gallery`,
    classCOLSmFour:`col-sm-4`,
    classBtn:`col-sm-12 col-12 col-xs-12 btn btn-primary o_list_button_add`,
    classImg:`asset-image-preview-2`,
    classTD:`pt-0`,
    isReadonly: false,
    image: undefined,
    image_id: 1,
    imgGallery: [],
    imagearray: [],
    name: undefined,
    actions: {
        upload: function (event) {
            var self = this;
            const reader = new FileReader();
            const file = event.target.files[0];
            var img_name = 'IMAGE_' + self.get("image_id").toString() + '.jpg'
            self.set('name', img_name)
            let imageData;

            reader.onload = function () {
                imageData = reader.result;
                var dict = {}
                dict['imageid'] = self.get("image_id")
                dict['imagename'] = img_name
                dict['imagedata'] = imageData
                self.get('imagearray').push(dict)
                self.set("imageData", imageData)
                self.set("image", imageData)
                self.get("imgGallery").push(imageData)
                self.set("image_id", self.get("image_id") + 1)
                self.send('photogallery')
            };
            if (file) {
                reader.readAsDataURL(file);
            }
        },

        photogallery() {
            var imagArray = this.get("imgGallery")
            var element_del = document.getElementById("gallery");
            element_del.remove()
            var row = document.createElement("tr");
            for (var i = 0; i < imagArray.length; i++) {
                var element_id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                var td = document.createElement("td")
                td.setAttribute("id", element_id)
                td.setAttribute("class", "pt-0")
                var image = document.createElement("img")
                image.setAttribute('src', imagArray[i])
                image.setAttribute('class', 'asset-image-preview-2')
                td.append(image)
                row.append(td)

            }
            var element = document.createElement("tbody")
            element.setAttribute("id", 'gallery')
            element.append(row)
            var table = document.getElementById("table_gal");
            table.append(element)


        },

    }
});
