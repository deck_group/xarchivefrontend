import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `tr`,
    keyaccess: `name`,
    store: service(),
    classTdOne: `col-sm-7 o_td_label`,
    classLabel: `o_form_label`,
    classTdTwo: `col-sm-5 padding-left`,
    classSelection: `form-control required`,
    selectionid: `ObjectId`,
    label: `Object Label`,
    store: service(),
    sortitem: `name`,
    orderby:`asc`,
    sortingByArray: computed(function () {
        var array = []
        var data=this.get("sortitem")+':'+this.get("orderby")
        array.push(data)
        return array
    }),
    model: computed(function () {
        return this.get('store').findAll(this.get('modelname'))
    }),
    sortedCollection: computed.sort('model', 'sortingByArray'),

    actions: {
        selectedOption(value) {
            this.set('setValue', value)
        }
    }

});
