import Component from '@ember/component';

export default Component.extend({
    tagName: `tr`,
    classNames: [`col-sm-12 maring-tp`],
    isReadonly: false,
    inputClass: "form-control required-3"
});
