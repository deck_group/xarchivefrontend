import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    width: 800,
    heigth: 600,
    tagName:`div`,
    className:[`col-sm-12`],
    imageurl:``,
    index:0,
    didInsertElement() {
        this.imagerender()
    },
    imagerender(){
        var img = new Image()
        var canvasremove = document.getElementById(this.get('index'))
        canvasremove.remove();
        var canvas = document.createElement("CANVAS");
        canvas.setAttribute("id", this.get("index"))
        canvas.setAttribute("width", `200`)
        canvas.setAttribute("height", `150`)
        var outer= `newcanvas_`+this.get("index").toString()
        document.getElementById(outer).append(canvas)

        img.src = this.get("imageurl")
        var ctx = canvas.getContext("2d")
        img.onload = function () {
            var hRatio = canvas.width / img.width;
            var vRatio = canvas.height / img.height;
            var ratio = Math.min(hRatio, vRatio);
            var centerShift_x = (canvas.width - img.width * ratio) / 2;
            var centerShift_y = (canvas.height - img.height * ratio) / 2;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(img, 0, 0, img.width, img.height,
                centerShift_x, centerShift_y, img.width * ratio, img.height * ratio)
        };

    }

});
