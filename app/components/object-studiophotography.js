import Component from '@ember/component';

export default Component.extend({
    tagName:`div`,
    classNames:[`row`],
    classDivOne:`col-sm-12 col-xs-12 col-12`,
    isReadonly:false,
    studiocaption:``,
    mode:``
});
