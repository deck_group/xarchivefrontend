import Component from '@ember/component';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Component.extend({
    tagName: `div`,
    urls: service("service"),
    data: [
        { 'text': 'Object' },
    ],
    classNames: [`row maringT`],
    classRow: `col-sm-12`,
    classRowMargin: `col-sm-12 marginR`,
    classbtnfull: `col-sm-12 col-12 col-xs-12 btn btn-primary o_list_button_add`,
    jstreeActionReceiver: undefined,
    jstreeSelectedNodes: ``,
    bus: service("pubsub"),
    urldirectory: undefined,
    classOverlayOne: `col-xs-6 col-sm-6 new-load`,
    classOverlayTwo: `col-xs-6 col-sm-6 login-card-2`,
    filename: undefined,
    totalnode: undefined,
    classInputTwo: `form-control input-txt-two`,
    classInputDiv: `col-sm-8`,
    classBtnDiv: `col-sm-4`,
    selectednode: undefined,
    selectedDirectory: ``,
    isSelected: false,
    mode: ``,
    parent: [],
    classBTNOne:`col-sm-12`,
    renamefile: undefined,
    message: undefined,
    delmessage: undefined,
    assetfilename: ``,
    classInput: `form-control required-directory`,
    byString(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1');
        s = s.replace(/^\./, '');
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    },

    recursion(father, newdata, totalnode) {
        if (father.length > 0) {
            var len = father.length
            while (len > 0) {
                var currentfather = father.shift()
                for (var v in totalnode) {
                    if (v !== '_super') {
                        if (totalnode[v]['parent'] === currentfather[`id`]) {
                            var rootdict = {}
                            rootdict[`id`] = totalnode[v]['id']
                            var dict = {}
                            dict['text'] = totalnode[v]['text']
                            if ('children' in this.byString(newdata, currentfather['path'])) {
                                var index = this.byString(newdata, currentfather['path'])['children'].length
                                rootdict[`path`] = currentfather['path'] + `.children[` + index + `]`
                            }
                            else {
                                this.byString(newdata, currentfather['path'])['children'] = []
                                rootdict[`path`] = currentfather['path'] + `.children[` + 0 + `]`
                            }
                            father.push(rootdict)

                            this.byString(newdata, currentfather['path'])['children'].push(dict)


                        }
                    }
                }

                len = len - 1

            }
            this.recursion(father, newdata, totalnode)
        }
        else {
            return newdata
        }

    },
    getNodeJsonID(node) {

    },

    addnewdata(parent_id, txt) {
        this.get('jstreeActionReceiver').send('getNode', parent_id)
        var currentparent = this.parentnode.id
        var previous = this.parentnode.parent
        var children_ids = this.parentnode.children
        var temparray = []
        if ('children' in this.data[0]) {
            if (currentparent === `j1_1`) {
                for (var i = 0; i < children_ids.length; i++) {
                    const found = this.data[0]['children'].some(el => el.id === children_ids[i])
                    if (found === false) {
                        var dict = {}
                        dict['text'] = txt
                        dict['id'] = children_ids[i]
                        dict['accesspath'] = `[0].children`
                        this.data[0]['children'].push(dict)
                    }
                }
            } else {
                for (var l = 0; l < this.data[0]['children'].length; l++) {
                    temparray.push(this.data[0]['children'][l])
                }
                for (var z = 0; z < temparray.length; z++) {
                    if (currentparent === temparray[z]['id']) {
                        for (var i = 0; i < children_ids.length; i++) {
                            var dict = {}
                            dict['text'] = txt
                            dict['id'] = children_ids[i]
                            dict['accesspath'] = temparray[z]['accesspath'] + `.children[` + i + `]`
                            if (`children` in this.byString(this.data, temparray[z]['accesspath'])) {
                                const found = this.byString(this.data, temparray[z]['accesspath'])['children'].some(el => el.id === children_ids[i])
                                if (found === false) {
                                    this.byString(this.data, temparray[z]['accesspath'])['children'].push(dict)
                                }
                            }
                            else {
                                this.byString(this.data, temparray[z]['accesspath'])['children'] = []
                                this.byString(this.data, temparray[z]['accesspath'])['children'].push(dict)
                            }
                        }
                    } else {

                    }
                }
            }

        }
        else {
            this.data[0]['children'] = []
            var dict = {}
            dict['text'] = txt
            dict['id'] = children_ids[0]
            dict['accesspath'] = `[0].children[0]`
            this.data[0]['children'].push(dict)
        }

    },


    getnodedata(data) {
        this.set("parentnode", data)
    },
    actions: {
        new() {
            var txt = ``
            var parent_id = ``
            this.set("isSelected", false)
            if (this.get("jstreeSelectedNodes").length < 1) {
                parent_id = this.get("jstreeObject")[0].childNodes[0].childNodes[0].id
                if (this.get("filename") !== undefined) {
                    txt = this.get("filename")
                }
                else {
                    txt = "Untitled"
                }
                this.get('jstreeActionReceiver').send('createNode', parent_id, { text: txt })
                this.get('jstreeActionReceiver').send('openAll');
            }
            else {

                if (this.get("filename") !== undefined) {
                    txt = this.get("filename")
                }
                else {
                    txt = "Untitled"
                }
                parent_id = this.get("jstreeSelectedNodes")[0].id
                this.get('jstreeActionReceiver').send('createNode', this.get("jstreeSelectedNodes")[0], txt)
                this.get('jstreeActionReceiver').send('openAll');

            }
            this.set("filename", undefined)
            this.send("set")

        },
        domData(node) {
            this.set("totalnode", node)

        },


        rename() {
            this.set("isSelected", false)
            if (this.get("jstreeSelectedNodes").length > 0) {
                if (this.get("renamefile") !== undefined || this.get("renamefile") !== '') {
                    if (this.get("jstreeSelectedNodes")[0].text !== "BAFArchive") {
                        this.get('jstreeActionReceiver').send('renameNode', this.get("jstreeSelectedNodes")[0], this.get("renamefile"))
                        this.get('jstreeActionReceiver').send('getPath', this.get("jstreeSelectedNodes")[0].id)
                        this.send("set")
                    } else {
                        this.set("renamefile", undefined)
                        this.set("message", `Cannot change the root name`)
                    }

                } else {
                    this.set("message", `Give a folder name`)
                }

            } else {
                this.set("message", `Select a folder to rename`)
            }
            this.set("renamefile", undefined)
            

        },
        delete() {
            this.set("isSelected", false)
            this.set("delmessage", undefined)
            if (this.get("jstreeSelectedNodes").length > 0) {
                if (this.get("jstreeSelectedNodes")[0].text !== "BAFArchive") {

                    this.get('jstreeActionReceiver').send('deleteNode', this.get("jstreeSelectedNodes")[0])
                    this.send("set")

                }
                else {
                    this.set("delmessage", `Cannot delete the root name`)
                }

            } else {
                this.set("delmessage", `Select a folder to rename`)
            }
            
        },
        clearMessage() {
            this.set("message", undefined)
            this.set("delmessage", undefined)
        },


        handleJstreePathNode(node) {
            var dir = `/`
            for (var i = 0; i < node.length; i++) {
                dir = dir + node[i] + `/`
            }
            this.set('selectedDirectory', dir)
            dir = dir + this.get('assetfilename')
            this.set("urldirectory", dir)
            this.get('bus').directoryvaluepub(dir);

        },
        handleJstreeGetNode(node) {
            this.getnodedata(node)
        },
        selectionNode(data) {
            if (this.get("mode") !== undefined) {
                this.get('jstreeActionReceiver').send('getPath', data)
            }

        },

        set() {
            var father = []
            var newdata = []
            this.get('jstreeActionReceiver').send('getJson');

            var totalnode = this.get("totalnode")
            console.log(totalnode)
            for (var v in totalnode) {
                if (v !== '_super') {
                    if (totalnode[v]['text'] === 'BAFArchive') {
                        var rootdict = {}
                        rootdict[`id`] = totalnode[v]['id']
                        rootdict[`path`] = `[0]`
                        father.push(rootdict)
                        var dict = {}
                        dict['text'] = totalnode[v]['text']
                        newdata.push(dict)

                    }
                }
            }
            this.recursion(father, newdata, totalnode)
            this.set(`data`, newdata)
            document.getElementById("overlay").style.display = "block";
            var hurl = this.get("urls.url") + '/insertgallery'
            $.ajax({
                method: "POST",
                url: hurl,
                data: {
                    data: JSON.stringify(newdata)

                }
            }).then(bind(this, (data) => {
                document.getElementById("overlay").style.display = "none";
            }))

        },

    }

});
