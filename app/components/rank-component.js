import Component from '@ember/component';
import {
    inject as service
} from '@ember/service';

export default Component.extend({
    store: service(),
    newmodel: {},
    actions: {
        save(newmodel) {
            var msg = " - "

            if (Object.keys(newmodel).length === 0) {
                msg = msg + "Please Provide name"
                this.send("errormsgdef", msg)

            }
            else {

                let newrank = this.get("store").createRecord("rank");
                newrank.set("name", newmodel.name)
                newrank.set("code", newmodel.code)
                newrank.set("description", newmodel.description)

                if (newrank.get('name') === undefined) {
                    if (newrank.get('name') === undefined) {
                        msg = msg + "Please Provide name"
                        this.send("errormsgdef", msg)
                    }
                }
                else {
                    console.log("else")
                    if (newrank.get('description') === undefined || newrank.get('description') === "") {
                        newrank.set('description', 'N/A')
                    }
                    if (newrank.get('code') === undefined || newrank.get('code') === "") {
                        newrank.set('code', 'N/A')
                    }
                    newrank.set('rankno', 0)
                    newrank.save().then(() => {
                        this.set('newmodel', {})
                        var ok = document.getElementById('discardRank');
                        ok.addEventListener("click", this.send("modalclose"));

                    })

                }


            }
        },

        modalclose() {
            var modal = document.getElementById('rankNew');
            this.get('tasksController').send("callRank")
            modal.style.display = 'none'
        },


        errormsgdef(msg) {
            var modal = document.getElementById('msg');
            var ok = document.getElementById('discardmsg');
            ok.onclick = function () {
                modal.style.display = 'none';
            }
            modal.style.display = 'block';
            document.getElementById('message').innerHTML = msg
        }
    }
});
