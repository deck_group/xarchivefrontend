import Component from '@ember/component';

export default Component.extend({
    tagName: `td`,
    classNames: [`col-sm-3`],
    classInput: `form-control input-txt-table`,
    isReadonly: false,
    data:``,
});
