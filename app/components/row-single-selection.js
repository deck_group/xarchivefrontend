import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `td`,
    classNames: [`col-sm-4`],
    classSelection: `rankselection`,
    store: service(),
    keyaccess: `name`,
    model: computed(function () {
        return this.get('store').findAll(this.get('modelname'))
    }),
    actions: {
        selectedOption(value) {
            this.set('data', value)
        }
    }



});
