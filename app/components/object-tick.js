import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `tr`,
    classTdOne: `col-sm-7 col-xs-12 col-12 o_td_label`,
    classLabel: `o_form_label`,
    label: `Object Title`,
    classTdTwo: `col-sm-5 col-xs-12 col-12 padding-left`,
    classInput: `form-control required`,
    ticked: false,
    holderflag:undefined,
    isBoardcast: false,
    bus: service("pubsub"),
    actions: {
        optionsChecked() {
            var flag = false
            if(document.querySelector('#retired:checked') !== null){
                flag=true
            }
            this.set("ticked",flag)
            if (this.get("isBoardcast") === true) {
                if(this.get("holderflag")!== undefined){
                    this.get('bus').sendImageCorrectionOption(flag,this.get("holderflag"));
                }
                else{
                    this.get('bus').sendAspectRatio(flag);
                }
                
            }

        }
    }
});
