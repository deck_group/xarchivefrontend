import Component from '@ember/component';

export default Component.extend({
    tagName:`tr`,
    tdClassOne: `col-sm-7 o_td_label`,
    labelClass:`o_form_label`,
    tdClassTwo:`col-sm-5 padding-left`,
    spanClass:`bigcheck`,
    spanLabelClass:`bigcheck`,
    inputClass:`bigcheck`,
    spanTargetClass:`bigcheck-target`,

    actions: {
        optionsChecked() { 
            this.set('data', document.getElementById("retired").checked)
        }
    }
});
