import Component from '@ember/component';

export default Component.extend({
    tagName: `tr`,
    spanClass:`bigcheck`,
    spanLabelClass:`bigcheck`,
    inputClass:`bigcheck`,
    spanTargetClass:`bigcheck-target`,
    autoisReadonly:false,
    titleReadonly:true,
    nameReadonly:true,
    mode:``,
});
