import Component from '@ember/component';
import objectTitleValidator from '../mixins/object-title-validator';


export default Component.extend(objectTitleValidator, {
    showErrors: true,
    tagName: 'tr',
    isReadonly: false,
    message: undefined,
    flagerror:false,
    format:"^[a-zA-Z0-9\\s.@-_']*$",
    isPresence: true,
    leasdingspace:true,
    label: "Object Title",
    tableLabelClass: "col-sm-4 padding-0 o_td_label",
    labelClass: "o_form_label",
    tdClass: "col-sm-7 padding-left",
    inputClass: "form-control required",
    minlength:0,
    maxlength:4096,
    mode : ``,
    actions: {
        setName() {
            this.set('message', undefined)
            this.set('message', 
                this.validate(this.get('title'),
                this.get('isPresence'),
                this.get('label'),
                this.get('format'),
                this.get('leasdingspace'),
                this.get('minlength'),
                this.get('maxlength')
                ))
            if(this.get('message').length>0){
                this.set('title',``)
            }

        },
        clearMessage() {
            this.set('message', undefined)
        },
    }
})