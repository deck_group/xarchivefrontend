import Component from '@ember/component';

export default Component.extend({
    tagName: 'tr',
    isReadonly: false,
    label: "Object Title",
    tableLabelClass: "col-sm-4 padding-0 o_td_label",
    labelClass: "o_form_label",
    tdClass: "col-sm-7 padding-left",
    classNames:[`newpadding`],
    cols:56,
    rows:2,
    data:``,
});
