import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    router: service(),
    actions: {
        logout() {
            localStorage.setItem("token", undefined)
            localStorage.setItem("user", undefined)
            localStorage.setItem('role', undefined)
            this.get('router').transitionTo('login')
        }
    }

});
