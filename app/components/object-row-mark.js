import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `td`,
    spanClass: `bigcheck`,
    spanLabelClass: `bigcheck`,
    inputClass: `bigcheck`,
    spanTargetClass: `bigcheck-target`,
    array: [],
    bus: service("pubsub"),
    keyaccess: `marked`,
    keytomatch: `sn`,
    actions: {
        optionsChecked() {
            var newarray = []
            for (var i = 0; i < this.get('array').length; i++) {
                if (this.get('sn') === this.get('array')[i][this.get('keytomatch')]) {
                    var recogdict = {}
                    Object.entries(this.get('array')[i]).forEach(([key, value]) => {
                        if (key === this.get('keyaccess')) {
                            console.log(this.get('array')[i][key])
                            if (this.get('array')[i][key] === true || this.get('array')[i][key] === "True") {
                                recogdict[key] = false
                                this.set('data', false)
                            } else {
                                recogdict[key] = true
                                this.set('data', true)
                            }
                        }
                        else {
                            recogdict[key] = value
                        }
                    });

                    newarray.push(recogdict)

                } else {
                    newarray.push(this.get('array')[i])
                }

            }
            this.get('bus').markedPublish(newarray);

        }
    }

});
