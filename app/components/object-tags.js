import Component from '@ember/component';

export default Component.extend({
    class:`div`,
    classNames:[`col-sm-3`],
    classDivOne:`col-sm-10`,
    classDivTwo:`col-sm-2 padding-gap`,
});
