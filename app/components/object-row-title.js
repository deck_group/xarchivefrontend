import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    bus: service("pubsub"),
    broadcast:false,
    init() {
        this._super();
        this.get('bus').on('newdata', this, this.recivedData);
    },
    titleCompute: computed('title', function () {
        return this.get('title')
    }),

    recivedData(data, id) {
        if (id === this.get('sn')) {
            this.set('title', data[this.get("keyaccess")])
            this.get('titleCompute')
            if(this.get("broadcast")===true){
                this.get('bus').assetPublish();

            }

        }

    },
    tagName: `td`,
    classNames: [`col-sm-2`],
    isReadonly: false,
    inputClass: "form-control required-3",
});
