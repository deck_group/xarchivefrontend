import Component from '@ember/component';
import { computed } from '@ember/object';


export default Component.extend({
    tagName: `div`,
    classNames: [`row`],
    array:[],
    classInput:`form-control input-txt-table-deep-orange`,
    classDivone:`col-sm-2`,
    classDivtwo:`col-sm-7 col-xs-7 col-7`,
    classTag:`tagstotal`,
    classRowTwo:`row margin-top-tab`,
    tableLabelClass: "o_td_label",
    classRow:`row`,
    mode:``,

    arrayComputed:computed('array', function () {
        return this.get('array')
    }),

    actions:{
        setvalue() {
            var dict = {}
            dict.name=this.get('data')
            this.get('array').pushObject(dict);
            this.set(`data`,undefined)
        }
    }
});
