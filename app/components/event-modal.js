import Component from '@ember/component';
import { computed } from '@ember/object';
import {
    inject as service
} from '@ember/service';

export default Component.extend({
    store: service(),
    objEvent: undefined,
    objDt: undefined,
    objDesc: undefined,
    newmodel: {},

    actions: {
        setTitle(value, obj) {
            this.set('objEvent', obj)
            this.set('newmodel.name', value)
        },

        setDate(value, obj) {
            this.set('objDt', obj)
            this.set('newmodel.date', value)
        },
        setDescription(value, obj) {
            this.set('objDesc', obj)
            this.set('newmodel.description', value)
        },
        save() {
            var newevent = this.get("store").createRecord("event")
            newevent.set("name", this.get('newmodel.name'))
            newevent.set("date", this.get('newmodel.date'))
            newevent.set("description", this.get('newmodel.description'))
            if (this.get('objEvent') !== undefined || this.get('objDt') !== undefined) {

                if (this.get('objDesc') === undefined) {
                    newevent.set("description", 'N/A')
                    this.send('saveModel', newevent)

                } else {

                    if (this.get('objDesc').get('flagerror') === false) {
                        if (newevent.get("description") == undefined) {
                            newevent.set("description", 'N/A')
                        }
                        this.send('saveModel', newevent)

                    }
                }




            }
        },

        saveModel(newevent) {
            newevent.save().then(() => {
                this.get('objEvent').set('title', undefined)
                this.get('objDt').set('dt', '')
                if (this.get('objDesc') !== undefined) { this.get('objDesc').set('desc', undefined) }
                this.set('newmodel', {})
                var ok = document.getElementById('discardEvent');
                ok.addEventListener("click", this.send("modalclose"));

            })
        },
        modalclose() {
            var modal = document.getElementById('eventNew');
            this.get('tasksController').send("callEvent")
            modal.style.display = 'none'
        },
    }
});
