"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _jquery = _interopRequireDefault(require("jquery"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  urls: (0, _service.inject)("service"),
  data: [{
    'text': 'Object'
  }],
  classOverlayOne: "col-xs-6 col-sm-6 new-load",
  classOverlayTwo: "col-xs-6 col-sm-6 login-card-2",
  jstreeActionReceiver: undefined,
  jstreeSelectedNodes: "",
  jstreeObject: "",
  tagName: "div",
  classNames: ["div-tree"],
  classTreeDiv: "col-sm-9",
  urldirectory: undefined,
  classTwoDiv: "col-sm-3",
  classBTN: "col-sm-2 col-2 col-xs-2",
  classBTNOne: "col-sm-10 col-10 col-xs-10",
  classInputDiv: "col-sm-10 col-10 col-xs-10",
  renamefile: undefined,
  message: undefined,
  delmessage: undefined,
  assetfilename: "",
  classInputTwo: "form-control input-txt-two",
  classInput: "form-control required-directory",
  classbtnfull: "col-sm-12 col-12 col-xs-12 btn btn-primary o_list_button_add",
  classRow: "row",
  filename: undefined,
  totalnode: undefined,
  byString: function byString(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1');
    s = s.replace(/^\./, '');
    var a = s.split('.');

    for (var i = 0, n = a.length; i < n; ++i) {
      var k = a[i];

      if (k in o) {
        o = o[k];
      } else {
        return;
      }
    }

    return o;
  },
  recursion: function recursion(father, newdata, totalnode) {
    if (father.length > 0) {
      var len = father.length;

      while (len > 0) {
        var currentfather = father.shift();

        for (var v in totalnode) {
          if (v !== '_super') {
            if (totalnode[v]['parent'] === currentfather["id"]) {
              var rootdict = {};
              rootdict["id"] = totalnode[v]['id'];
              var dict = {};
              dict['text'] = totalnode[v]['text'];

              if ('children' in this.byString(newdata, currentfather['path'])) {
                var index = this.byString(newdata, currentfather['path'])['children'].length;
                rootdict["path"] = currentfather['path'] + ".children[" + index + "]";
              } else {
                this.byString(newdata, currentfather['path'])['children'] = [];
                rootdict["path"] = currentfather['path'] + ".children[" + 0 + "]";
              }

              father.push(rootdict);
              this.byString(newdata, currentfather['path'])['children'].push(dict);
            }
          }
        }

        len = len - 1;
      }

      this.recursion(father, newdata, totalnode);
    } else {
      return newdata;
    }
  },
  addnewdata: function addnewdata(parent_id, txt) {
    this.get('jstreeActionReceiver').send('getNode', parent_id);
    var currentparent = this.parentnode.id;
    var previous = this.parentnode.parent;
    var children_ids = this.parentnode.children;
    var temparray = [];

    if ('children' in this.data[0]) {
      if (currentparent === "j1_1") {
        for (var i = 0; i < children_ids.length; i++) {
          var found = this.data[0]['children'].some(function (el) {
            return el.id === children_ids[i];
          });

          if (found === false) {
            var dict = {};
            dict['text'] = txt;
            dict['id'] = children_ids[i];
            dict['accesspath'] = "[0].children";
            this.data[0]['children'].push(dict);
          }
        }
      } else {
        for (var l = 0; l < this.data[0]['children'].length; l++) {
          temparray.push(this.data[0]['children'][l]);
        }

        for (var z = 0; z < temparray.length; z++) {
          if (currentparent === temparray[z]['id']) {
            for (var i = 0; i < children_ids.length; i++) {
              var dict = {};
              dict['text'] = txt;
              dict['id'] = children_ids[i];
              dict['accesspath'] = temparray[z]['accesspath'] + ".children[" + i + "]";

              if ("children" in this.byString(this.data, temparray[z]['accesspath'])) {
                var _found = this.byString(this.data, temparray[z]['accesspath'])['children'].some(function (el) {
                  return el.id === children_ids[i];
                });

                if (_found === false) {
                  this.byString(this.data, temparray[z]['accesspath'])['children'].push(dict);
                }
              } else {
                this.byString(this.data, temparray[z]['accesspath'])['children'] = [];
                this.byString(this.data, temparray[z]['accesspath'])['children'].push(dict);
              }
            }
          } else {}
        }
      }
    } else {
      this.data[0]['children'] = [];
      var dict = {};
      dict['text'] = txt;
      dict['id'] = children_ids[0];
      dict['accesspath'] = "[0].children[0]";
      this.data[0]['children'].push(dict);
    }
  },
  getnodedata: function getnodedata(data) {
    this.set("parentnode", data);
  },
  actions: {
    "new": function _new() {
      var txt = "";
      var parent_id = "";

      if (this.get("jstreeSelectedNodes").length < 1) {
        parent_id = this.get("jstreeObject")[0].childNodes[0].childNodes[0].id;

        if (this.get("filename") !== undefined) {
          txt = this.get("filename");
        } else {
          txt = "Untitled";
        }

        this.get('jstreeActionReceiver').send('createNode', parent_id, {
          text: txt
        });
        this.get('jstreeActionReceiver').send('openAll');
      } else {
        if (this.get("filename") !== undefined) {
          txt = this.get("filename");
        } else {
          txt = "Untitled";
        }

        parent_id = this.get("jstreeSelectedNodes")[0].id;
        this.get('jstreeActionReceiver').send('createNode', this.get("jstreeSelectedNodes")[0], txt);
        this.get('jstreeActionReceiver').send('openAll');
      } // this.addnewdata(parent_id, txt)


      this.set("filename", undefined);
    },
    domData: function domData(node) {
      this.set("totalnode", node);
    },
    rename: function rename() {
      if (this.get("jstreeSelectedNodes").length > 0) {
        if (this.get("renamefile") !== undefined || this.get("renamefile") !== '') {
          if (this.get("jstreeSelectedNodes")[0].text !== "BAFArchive") {
            this.get('jstreeActionReceiver').send('renameNode', this.get("jstreeSelectedNodes")[0], this.get("renamefile"));
            this.get('jstreeActionReceiver').send('getPath', this.get("jstreeSelectedNodes")[0].id);
          } else {
            this.set("renamefile", undefined);
            this.set("message", "Cannot change the root name");
          }
        } else {
          this.set("message", "Give a folder name");
        }
      } else {
        this.set("message", "Select a folder to rename");
      }

      this.set("renamefile", undefined);
    },
    "delete": function _delete() {
      this.set("delmessage", undefined);

      if (this.get("jstreeSelectedNodes").length > 0) {
        if (this.get("jstreeSelectedNodes")[0].text !== "BAFArchive") {
          this.get('jstreeActionReceiver').send('deleteNode', this.get("jstreeSelectedNodes")[0]);
        } else {
          this.set("delmessage", "Cannot delete the root name");
        }
      } else {
        this.set("delmessage", "Select a folder to rename");
      }
    },
    clearMessage: function clearMessage() {
      this.set("message", undefined);
      this.set("delmessage", undefined);
    },
    handleJstreePathNode: function handleJstreePathNode(node) {
      var dir = "/";

      for (var i = 0; i < node.length; i++) {
        dir = dir + node[i] + "/";
      }

      dir = dir + this.get('assetfilename') + '.jpg';
      this.set("urldirectory", dir);
    },
    handleJstreeGetNode: function handleJstreeGetNode(node) {
      this.getnodedata(node);
    },
    selectionNode: function selectionNode(data) {
      this.get('jstreeActionReceiver').send('getPath', data);
    },
    set: function set() {
      var father = [];
      var newdata = [];
      this.get('jstreeActionReceiver').send('getJson');
      var totalnode = this.get("totalnode");

      for (var v in totalnode) {
        if (v !== '_super') {
          if (totalnode[v]['id'] === 'j1_1') {
            var rootdict = {};
            rootdict["id"] = totalnode[v]['id'];
            rootdict["path"] = "[0]";
            father.push(rootdict);
            var dict = {};
            dict['text'] = totalnode[v]['text'];
            newdata.push(dict);
          }
        }
      }

      this.recursion(father, newdata, totalnode);
      this.set("data", newdata);
      document.getElementById("overlay").style.display = "block";
      var hurl = this.get("urls.url") + '/insertgallery';

      _jquery["default"].ajax({
        method: "POST",
        url: hurl,
        data: {
          data: JSON.stringify(newdata)
        }
      }).then((0, _runloop.bind)(this, function (data) {
        document.getElementById("overlay").style.display = "none";
      }));
    }
  }
});

exports["default"] = _default;