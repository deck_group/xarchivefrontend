"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  "class": "div",
  classNames: ["col-sm-3"],
  classDivOne: "col-sm-10",
  classDivTwo: "col-sm-2"
});

exports["default"] = _default;