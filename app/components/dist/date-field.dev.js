"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _dateFieldValidator = _interopRequireDefault(require("../mixins/date-field-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_dateFieldValidator["default"], {
  tagName: 'tr',
  isReadonly: false,
  fieldtype: 'date',
  label: "Object Title",
  tableLabelClass: "col-sm-4 padding-0 o_td_label",
  labelClass: "o_form_label",
  message: undefined,
  tdClass: "col-sm-7 padding-left",
  isPresence: true,
  before: new Date(),
  flagerror: false,
  after: '01/01/1900',
  inputClass: "form-control required",
  actions: {
    setDate: function setDate() {
      this.set('message', undefined);
      this.set('message', this.validate(this.get('dt'), this.get('isPresence'), this.get('label'), this.get('after'), this.get('before')));

      if (this.get('message').length > 0) {
        this.set('flagerror', true);
      }

      if (this.get('flagerror') === false) {// this.attrs.changeDate(this.get('dt'), this)
      }
    }
  }
});

exports["default"] = _default;