"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

var _Component$extend;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = _component["default"].extend((_Component$extend = {
  tagName: "tr",
  keyaccess: "name",
  store: (0, _service.inject)(),
  classTdOne: "col-sm-7 o_td_label",
  classLabel: "o_form_label",
  classTdTwo: "col-sm-5 padding-left",
  classSelection: "form-control required",
  selectionid: "ObjectId",
  label: "Object Label"
}, _defineProperty(_Component$extend, "store", (0, _service.inject)()), _defineProperty(_Component$extend, "sortitem", "name"), _defineProperty(_Component$extend, "orderby", "asc"), _defineProperty(_Component$extend, "sortingByArray", (0, _object.computed)(function () {
  var array = [];
  var data = this.get("sortitem") + ':' + this.get("orderby");
  array.push(data);
  return array;
})), _defineProperty(_Component$extend, "model", (0, _object.computed)(function () {
  return this.get('store').findAll(this.get('modelname'));
})), _defineProperty(_Component$extend, "sortedCollection", _object.computed.sort('model', 'sortingByArray')), _defineProperty(_Component$extend, "actions", {
  selectedOption: function selectedOption(value) {
    this.set('setValue', value);
  }
}), _Component$extend));

exports["default"] = _default;