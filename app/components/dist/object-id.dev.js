"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _objectIdValidator = _interopRequireDefault(require("../mixins/object-id-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectIdValidator["default"], {
  tagName: "tr",
  classTDOne: "col-sm-5 col-xs-12 col-12 o_td_label",
  label: "Object Title",
  classTdTwo: "col-sm-7 col-xs-12 col-12 padding-left",
  classInput: "form-control required",
  isReadonly: false,
  minlength: 5,
  rowid: "Object id",
  isPresence: true,
  maxlength: 500,
  placeholder: "",
  format: "^[a-zA-Z0-9\\s-_']*$",
  actions: {
    checkID: function checkID() {
      if (this.get("isReadonly") === false) {
        this.set('message', undefined);
        this.set('message', this.validate(this.get('data'), this.get('isPresence'), this.get('label'), this.get('format'), this.get('leasdingspace'), this.get('minlength'), this.get('maxlength')));
      }

      if (this.get('message').length > 0) {
        this.set('data', "");
      }
    },
    clearMessage: function clearMessage() {
      this.set('message', undefined);
    }
  }
});

exports["default"] = _default;