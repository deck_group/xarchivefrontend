"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  broadcast: false,
  init: function init() {
    this._super();

    this.get('bus').on('newdata', this, this.recivedData);
  },
  titleCompute: (0, _object.computed)('title', function () {
    return this.get('title');
  }),
  recivedData: function recivedData(data, id) {
    if (id === this.get('sn')) {
      this.set('title', data[this.get("keyaccess")]);
      this.get('titleCompute');

      if (this.get("broadcast") === true) {
        this.get('bus').assetPublish();
      }
    }
  },
  tagName: "td",
  classNames: ["col-sm-2"],
  isReadonly: false,
  inputClass: "form-control required-3"
});

exports["default"] = _default;