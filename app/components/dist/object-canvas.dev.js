"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _jquery = _interopRequireDefault(require("jquery"));

var _runloop = require("@ember/runloop");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var _default = _component["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  router: (0, _service.inject)(),
  storage: (0, _service.inject)("storage"),
  init: function init() {
    this._super();

    this.get('bus').on('revisedrecognize', this, this.recivedData);
    this.get('bus').on('markedrecognize', this, this.recivedDataMarked);
    this.get('bus').on('recogA', this, this.recivedData);
  },
  tagName: "div",
  parameter: undefined,
  route: undefined,
  classOverlayOne: "col-xs-6 col-sm-6 new-load",
  classOverlayTwo: "col-xs-6 col-sm-6 login-card-2",
  classNames: ["col-sm-8 col-xs-12 col-12"],
  classColSmall: "col-sm-2 col-xs-12 col-12",
  classColEigth: "col-sm-8 col-xs-12 col-12",
  urls: (0, _service.inject)("service"),
  classOuter: "row outerContainer",
  classColSmTwo: "col-sm-2 col-xs-12 col-12",
  classColSmSix: "col-sm-6 col-xs-12 col-12",
  inputClass: "col-sm-12 col-xs-12 col-12 form-control",
  width: 800,
  heigth: 600,
  imgw: 0,
  imgh: 0,
  image_ratio: 0.0,
  isUpload: true,
  delta_x: 0,
  delta_y: 0,
  recogArray: [],
  imageurl: "",
  classCOlSM5: "col-sm-4 col-xs-12 col-12 middle",
  ClassColSMFour: "col-sm-5 col-xs-12 col-12",
  label: "Object Title",
  buttonClasstwo: "col-sm-4 col-12 col-xs-12 btn btn-primary o_list_button_add",
  buttonClass: "col-sm-2 col-12 col-xs-12 btn btn-primary o_list_button_add",
  classRow: "row",
  newrect: [],
  ifimageEdited: false,
  image: undefined,
  edit: false,
  didInsertElement: function didInsertElement() {
    if (this.get("isUpload") === false) {
      this.imagetorender();
    } else {
      if (this.get("edit") === true) {
        this.imagetorenderedit();
      } else if (this.get("ifimageEdited") === true) {
        this.imagetorendereditnew();
      }
    }
  },
  imagetorendereditnew: function imagetorendereditnew() {
    var self = this;
    var drag = false;
    var rect = {};
    var canvasremove = document.getElementById("myCanvas");
    canvasremove.remove();
    var canvas = document.createElement("CANVAS");
    canvas.setAttribute("id", "myCanvas");
    canvas.setAttribute("width", "800");
    canvas.setAttribute("height", "600");
    document.getElementById("newcanvas").append(canvas);
    var ctx = canvas.getContext("2d");
    var img = new Image();
    var result = this.get('recogArray');

    img.onload = function () {
      var hRatio = canvas.width / this.width;
      var vRatio = canvas.height / this.height;
      var ratio = Math.min(hRatio, vRatio);
      var centerShift_x = (canvas.width - this.width * ratio) / 2;
      var centerShift_y = (canvas.height - this.height * ratio) / 2;
      self.set('delta_x', centerShift_x);
      self.set('delta_y', centerShift_y);
      self.set('image_ratio', ratio);
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, 0, 0, this.width, this.height, centerShift_x, centerShift_y, this.width * ratio, this.height * ratio);

      for (var z = 0; z < result.length; z++) {
        var canvas2 = document.getElementById('myCanvas');
        var context = canvas2.getContext('2d');
        var context = canvas.getContext("2d");
        context.strokeStyle = "#FF2600";
        context.lineWidth = 5;
        context.strokeRect(parseFloat(result[z]['x']), parseFloat(result[z]['y']), parseFloat(result[z]['width']), parseFloat(result[z]['height']));
        context.fillStyle = "#ff2800";
        context.font = "700 12px Arial";
        var text = result[z]['sn'] + '.' + result[z]['bdno'];
        context.fillText(text, result[z]['x'], result[z]['y'] - 20);
      }
    };

    img.src = this.get("image");

    if (result.length > 0) {
      document.getElementById("retired").checked = true;
    }

    canvas.addEventListener('mousedown', function (e) {
      var rect3 = canvas.getBoundingClientRect();
      rect.startX = e.clientX - rect3.left;
      rect.startY = e.clientY - rect3.top;
      drag = true;
    }, false);
    canvas.addEventListener('mouseup', function (e) {
      drag = false;
      self.send("draw");
    }, false);
    canvas.addEventListener('mousemove', function (e) {
      if (drag) {
        self.set('newrect', []);
        var rect3 = canvas.getBoundingClientRect();
        var img = new Image();
        img.src = self.get('image');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, img.width, img.height, self.get('delta_x'), self.get('delta_y'), img.width * self.get('image_ratio'), img.height * self.get('image_ratio'));
        rect.w = e.clientX - rect3.left - rect.startX;
        rect.h = e.clientY - rect3.top - rect.startY;
        ctx.strokeStyle = "#FF2600";
        ctx.lineWidth = 5;
        ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);
        var result = self.get('recogArray');

        for (var z = 0; z < result.length; z++) {
          ctx.strokeStyle = "#FF2600";
          ctx.lineWidth = 5;
          ctx.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
          ctx.fillStyle = "#ff2800";
          ctx.font = "700 12px Arial";
          var text = result[z]['sn'] + '.' + result[z]['bdno'];
          ctx.fillText(text, result[z]['x'], result[z]['y'] - 20);
        }

        var newrect = {};
        var lastsn = 0;

        if (result.length > 0) {
          lastsn = result[result.length - 1]["sn"];
        }

        newrect['x'] = rect.startX;
        newrect['y'] = rect.startY;
        newrect['width'] = rect.w;
        newrect['height'] = rect.h;
        newrect['sn'] = parseInt(lastsn) + 1;
        newrect['bdno'] = "";
        newrect['name'] = "";
        newrect['rank'] = "";
        newrect['nameplate'] = "";
        newrect['marked'] = true;
        newrect['rankinpic'] = "";
        newrect['isManush'] = true;
        self.get('newrect').push(newrect);
      }
    }, false);
  },
  imagetorenderedit: function imagetorenderedit() {
    var self = this;
    var drag = false;
    var rect = {};
    var canvasremove = document.getElementById("myCanvas");
    canvasremove.remove();
    var canvas = document.createElement("CANVAS");
    canvas.setAttribute("id", "myCanvas");
    canvas.setAttribute("width", "800");
    canvas.setAttribute("height", "600");
    document.getElementById("newcanvas").append(canvas);
    var ctx = canvas.getContext("2d");
    var img = new Image();
    var result = this.get('recogArray');
    var array = [];

    img.onload = function () {
      var hRatio = canvas.width / this.width;
      var vRatio = canvas.height / this.height;
      var ratio = Math.min(hRatio, vRatio);
      var centerShift_x = (canvas.width - this.width * ratio) / 2;
      var centerShift_y = (canvas.height - this.height * ratio) / 2;
      self.set('delta_x', centerShift_x);
      self.set('delta_y', centerShift_y);
      self.set('image_ratio', ratio);
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, 0, 0, this.width, this.height, centerShift_x, centerShift_y, this.width * ratio, this.height * ratio);

      for (var z = 0; z < result.length; z++) {
        var canvas2 = document.getElementById('myCanvas');
        var context = canvas2.getContext('2d');
        var context = canvas.getContext("2d");
        context.strokeStyle = "#FF2600";
        context.lineWidth = 5;
        context.strokeRect(parseFloat(result[z]['x']), parseFloat(result[z]['y']), parseFloat(result[z]['width']), parseFloat(result[z]['height']));
        context.fillStyle = "#ff2800";
        context.font = "700 12px Arial";
        var text = result[z]['sn'] + '.' + result[z]['bdno'];
        context.fillText(text, result[z]['x'], result[z]['y'] - 20);
      }
    };

    img.src = this.get("imageurl");

    if (result.length > 0) {
      document.getElementById("retired").checked = true;
    }

    canvas.addEventListener('mousedown', function (e) {
      var rect3 = canvas.getBoundingClientRect();
      rect.startX = e.clientX - rect3.left;
      rect.startY = e.clientY - rect3.top;
      drag = true;
    }, false);
    canvas.addEventListener('mouseup', function (e) {
      drag = false;
      self.send("draw");
    }, false);
    canvas.addEventListener('mousemove', function (e) {
      if (drag) {
        self.set('newrect', []);
        var rect3 = canvas.getBoundingClientRect();
        var img = new Image();
        img.src = self.get('image');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, img.width, img.height, self.get('delta_x'), self.get('delta_y'), img.width * self.get('image_ratio'), img.height * self.get('image_ratio'));
        rect.w = e.clientX - rect3.left - rect.startX;
        rect.h = e.clientY - rect3.top - rect.startY;
        ctx.strokeStyle = "#FF2600";
        ctx.lineWidth = 5;
        ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);
        var result = self.get('recogArray');

        for (var z = 0; z < result.length; z++) {
          ctx.strokeStyle = "#FF2600";
          ctx.lineWidth = 5;
          ctx.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
          ctx.fillStyle = "#ff2800";
          ctx.font = "700 12px Arial";
          var text = result[z]['sn'] + '.' + result[z]['bdno'];
          ctx.fillText(text, result[z]['x'], result[z]['y'] - 20);
        }

        var newrect = {};
        var lastsn = 0;

        if (result.length > 0) {
          lastsn = result[result.length - 1]["sn"];
        }

        newrect['x'] = rect.startX;
        newrect['y'] = rect.startY;
        newrect['width'] = rect.w;
        newrect['height'] = rect.h;
        newrect['sn'] = parseInt(lastsn) + 1;
        newrect['bdno'] = "";
        newrect['name'] = "";
        newrect['rank'] = "";
        newrect['nameplate'] = "";
        newrect['marked'] = true;
        newrect['rankinpic'] = "";
        newrect['isManush'] = true;
        self.get('newrect').push(newrect);
      }

      this.get('bus').recogpublish(this.get('recogArray'));
    }, false);
  },
  imagetorender: function imagetorender() {
    var self = this;
    var canvasremove = document.getElementById("myCanvas");
    canvasremove.remove();
    var canvas = document.createElement("CANVAS");
    canvas.setAttribute("id", "myCanvas");
    canvas.setAttribute("width", "800");
    canvas.setAttribute("height", "600");
    document.getElementById("newcanvas").append(canvas);
    var ctx = canvas.getContext("2d");
    var img = new Image();
    var result = this.get('recogArray');
    var array = [];

    img.onload = function () {
      var hRatio = canvas.width / this.width;
      var vRatio = canvas.height / this.height;
      var ratio = Math.min(hRatio, vRatio);
      var centerShift_x = (canvas.width - this.width * ratio) / 2;
      var centerShift_y = (canvas.height - this.height * ratio) / 2;
      self.set('delta_x', centerShift_x);
      self.set('delta_y', centerShift_y);
      self.set('image_ratio', ratio);
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, 0, 0, this.width, this.height, centerShift_x, centerShift_y, this.width * ratio, this.height * ratio);

      for (var z = 0; z < result.length; z++) {
        var canvas2 = document.getElementById('myCanvas');
        var context = canvas2.getContext('2d');
        var context = canvas.getContext("2d");
        context.strokeStyle = "#FF2600";
        context.lineWidth = 5;
        context.strokeRect(parseFloat(result[z]['x']), parseFloat(result[z]['y']), parseFloat(result[z]['width']), parseFloat(result[z]['height']));
        context.fillStyle = "#ff2800";
        context.font = "700 12px Arial";
        var text = result[z]['sn'] + '.' + result[z]['bdno'];
        context.fillText(text, result[z]['x'], result[z]['y'] - 20);
      }
    };

    img.src = this.get("imageurl");
    img.id = "image";

    if (result.length > 0) {
      document.getElementById("retired").checked = true;
    }
  },
  recivedData: function recivedData(data) {
    var that = this;
    var result = data;
    this.set('recogArray', undefined);
    this.set('recogArray', result);
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, 800, 600);
    var img = new Image();
    img.src = this.get('image');
    img.id = "image";

    img.onload = function () {
      ctx.clearRect(0, 0, c.width, c.height);
      ctx.drawImage(img, 0, 0, img.width, img.height, that.get('delta_x'), that.get('delta_y'), img.width * that.get('image_ratio'), img.height * that.get('image_ratio'));

      if (result.length > 0) {
        for (var z = 0; z < result.length; z++) {
          var canvas = document.getElementById('myCanvas');
          var context = canvas.getContext('2d');
          context.strokeStyle = "#FF2600";
          context.lineWidth = 5;
          context.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
          context.fillStyle = "#ff2800";
          context.font = "700 12px Arial";
          var text = result[z]['sn'] + '.' + result[z]['bdno'];
          context.fillText(text, result[z]['x'], result[z]['y'] - 20);
        }
      } else {
        document.getElementById("retired").checked = false;
      }
    };
  },
  recivedDataMarked: function recivedDataMarked(data) {
    var that = this;
    var result = data;
    var imagedata = "";

    if (this.get("isUpload") === false) {
      imagedata = this.get("imageurl");
    } else {
      imagedata = this.get('image');
    }

    this.set('recogArray', undefined);
    this.set('recogArray', result);
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, 800, 600);
    var img = new Image();
    img.src = imagedata;

    img.onload = function () {
      ctx.clearRect(0, 0, c.width, c.height);
      ctx.drawImage(img, 0, 0, img.width, img.height, that.get('delta_x'), that.get('delta_y'), img.width * that.get('image_ratio'), img.height * that.get('image_ratio'));

      for (var z = 0; z < result.length; z++) {
        if (result[z]['marked'] === true || result[z]['marked'] === "True") {
          var canvas = document.getElementById('myCanvas');
          var context = canvas.getContext('2d');
          context.strokeStyle = "#FF2600";
          context.lineWidth = 5;
          context.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
          context.fillStyle = "#ff2800";
          context.font = "700 12px Arial";
          var text = result[z]['sn'] + '.' + result[z]['bdno'];
          context.fillText(text, result[z]['x'], result[z]['y'] - 20);
        }
      }
    };
  },
  actions: {
    imageedit: function imageedit() {
      var img = this.get('image');
      var im = new Image();
      im.src = img;
      this.get('storage').set("parameter", this.get("parameter"));
      this.get('storage').set("route", this.get("route"));

      if (img !== undefined) {
        if (this.get("imgw") > 0) {
          this.get('storage').set("width", this.get("imgw"));
        } else {
          this.get('storage').set("width", im.width);
        }

        if (this.get("imgh") > 0) {
          this.get('storage').set("height", this.get("imgh"));
        } else {
          this.get('storage').set("height", im.height);
        }

        this.get('storage').set("imagestorage", img);
        this.get('router').transitionTo('imageeditor');
      }
    },
    upload: function upload(event) {
      var self = this;
      self.set('recogArray', []);
      var reader = new FileReader();
      var file = event.target.files[0];
      var canvasremove = document.getElementById("myCanvas");
      canvasremove.remove();
      var canvas = document.createElement("CANVAS");
      canvas.setAttribute("id", "myCanvas");
      canvas.setAttribute("width", "800");
      canvas.setAttribute("height", "600");
      document.getElementById("newcanvas").append(canvas);
      var ctx = canvas.getContext("2d");
      var imageData;
      this.get('storage').set("imagestorage", "");
      this.get('storage').set("brigthness", 0);
      this.get('storage').set("width", 0);
      this.get('storage').set("height", 0);
      this.get('storage').set("hue", 0);
      this.get('storage').set("saturation", 100);
      this.get('storage').set("lighness", 50);

      reader.onload = function () {
        imageData = reader.result;
        self.set("image", imageData);
        var img = new Image();
        img.src = imageData;

        img.onload = function () {
          var hRatio = canvas.width / img.width;
          var vRatio = canvas.height / img.height;
          var ratio = Math.min(hRatio, vRatio);
          var centerShift_x = (canvas.width - img.width * ratio) / 2;
          var centerShift_y = (canvas.height - img.height * ratio) / 2;
          self.set('delta_x', centerShift_x);
          self.set('delta_y', centerShift_y);
          self.set('image_ratio', ratio);
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);
        };
      };

      if (file) {
        reader.readAsDataURL(file);
      }
    },
    optionsChecked: function optionsChecked() {
      var that = this;
      var imagedata = "";

      if (this.get("isUpload") === false) {
        imagedata = this.get("imageurl");
      } else {
        imagedata = this.get('image');
      }

      if (document.getElementById("retired").checked === false) {
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, 800, 600);
        var img = new Image();
        img.src = imagedata;

        img.onload = function () {
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.drawImage(img, 0, 0, img.width, img.height, that.get('delta_x'), that.get('delta_y'), img.width * that.get('image_ratio'), img.height * that.get('image_ratio'));
        };

        var result = this.get('recogArray');
        this.set('recogArray', []);

        for (var z = 0; z < result.length; z++) {
          var recogdict = {};
          recogdict['x'] = result[z]['x'];
          recogdict['y'] = result[z]['y'];
          recogdict['width'] = result[z]['width'];
          recogdict['height'] = result[z]['height'];
          recogdict['sn'] = result[z]['sn'];
          recogdict['bdno'] = result[z]['bdno'];
          recogdict['name'] = result[z]['name'];
          recogdict['rank'] = result[z]['rank'];
          recogdict['nameplate'] = result[z]['nameplate'];
          recogdict['marked'] = false;
          recogdict['rankinpic'] = result[z]['rankinpic'];
          recogdict['isManush'] = result[z]['isManush'];
          this.get('recogArray').push(recogdict);
        }
      } else {
        var result = this.get('recogArray');
        this.set('recogArray', []);

        for (var z = 0; z < result.length; z++) {
          var canvas = document.getElementById("myCanvas");
          var ctx = canvas.getContext("2d");
          ctx.strokeStyle = "#FF2600";
          ctx.lineWidth = 5;
          ctx.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
          ctx.fillStyle = "#ff2800";
          ctx.font = "700 12px Arial";
          var text = result[z]['sn'] + '.' + result[z]['bdno'];
          ctx.fillText(text, result[z]['x'], result[z]['y'] - 20);
          var recogdict = {};
          recogdict['x'] = result[z]['x'];
          recogdict['y'] = result[z]['y'];
          recogdict['width'] = result[z]['width'];
          recogdict['height'] = result[z]['height'];
          recogdict['sn'] = result[z]['sn'];
          recogdict['bdno'] = result[z]['bdno'];
          recogdict['name'] = result[z]['name'];
          recogdict['rank'] = result[z]['rank'];
          recogdict['nameplate'] = result[z]['nameplate'];
          recogdict['marked'] = true;
          recogdict['rankinpic'] = result[z]['rankinpic'];
          recogdict['isManush'] = result[z]['isManush'];
          this.get('recogArray').push(recogdict);
        }
      }

      this.get('bus').recogpublish(this.get('recogArray'));
    },
    recognization: function recognization() {
      var _this = this;

      this.set('recogArray', []);
      this.get('bus').recogpublish(this.get('recogArray'));

      if (this.get("image") !== undefined) {
        this.set('recogArray', []);
        document.getElementById("retired").checked = false;
        var hurl = this.get("urls.url") + '/imageresults';
        document.getElementById("overlay").style.display = "block";

        _jquery["default"].ajax({
          method: "POST",
          url: hurl,
          data: {
            imagedata: this.get('image')
          }
        }).then((0, _runloop.bind)(this, function (data) {
          document.getElementById("overlay").style.display = "none";
          var rect = {};
          var drag = false;
          var self = _this;
          var result = data["result"];
          var canvas = document.getElementById("myCanvas");
          var ctx = canvas.getContext("2d");

          for (var z = 0; z < result.length; z++) {
            var fiWidth = result[z]["iwidth"] * _this.get("image_ratio");

            var fiHeight = result[z]["iheight"] * _this.get("image_ratio");

            var tleft = fiWidth / result[z]["iwidth"] * result[z]["left"];
            var ttop = fiHeight / result[z]["iheight"] * result[z]["top"];
            var tright = fiWidth / result[z]["iwidth"] * result[z]["right"];
            var tbottom = fiHeight / result[z]["iheight"] * result[z]["bottom"];

            if (fiWidth > fiHeight) {
              ttop = ttop + _this.get('delta_y');
              tbottom = tbottom + _this.get('delta_y');
            } else if (fiWidth < fiHeight) {
              tleft = tleft + _this.get('delta_x');
              tright = tright + _this.get('delta_x');
            } else {
              tleft = tleft + _this.get('delta_x');
              tright = tright + _this.get('delta_x');
            }

            var x = tleft;
            var y = ttop;
            var w = tright - tleft;
            var h = tbottom - ttop;
            ctx.strokeStyle = "#FF2600";
            ctx.lineWidth = 5;
            ctx.strokeRect(x, y, w, h);
            ctx.fillStyle = "#ff2800";
            ctx.font = "700 12px Arial";
            var text = result[z]['sn'] + '.' + result[z]['bdno'];
            ctx.fillText(text, x, y - 20);
            var recogdict = {};
            recogdict['x'] = x;
            recogdict['y'] = y;
            recogdict['width'] = w;
            recogdict['height'] = h;
            recogdict['sn'] = result[z]['sn'];
            recogdict['bdno'] = result[z]['bdno'];
            recogdict['name'] = result[z]['name'];
            recogdict['rank'] = result[z]['rank'];
            recogdict['nameplate'] = result[z]['nameplate'];
            recogdict['marked'] = true;
            recogdict['rankinpic'] = "";
            recogdict['isManush'] = false;

            _this.set("imgw", result[z]["iwidth"]);

            _this.set("imgh", result[z]["iheight"]);

            _this.get('recogArray').push(recogdict);
          }

          document.getElementById("retired").checked = true;
          canvas.addEventListener('mousedown', function (e) {
            var rect3 = canvas.getBoundingClientRect();
            rect.startX = e.clientX - rect3.left;
            rect.startY = e.clientY - rect3.top;
            drag = true;
          }, false);
          canvas.addEventListener('mouseup', function (e) {
            drag = false;
            self.send("draw");
          }, false);
          canvas.addEventListener('mousemove', function (e) {
            if (drag) {
              self.set('newrect', []);
              var rect3 = canvas.getBoundingClientRect();
              var img = new Image();
              img.src = self.get('image');
              ctx.clearRect(0, 0, canvas.width, canvas.height);
              ctx.drawImage(img, 0, 0, img.width, img.height, self.get('delta_x'), self.get('delta_y'), img.width * self.get('image_ratio'), img.height * self.get('image_ratio'));
              rect.w = e.clientX - rect3.left - rect.startX;
              rect.h = e.clientY - rect3.top - rect.startY;
              ctx.strokeStyle = "#FF2600";
              ctx.lineWidth = 5;
              ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);
              var result = self.get('recogArray');

              for (var z = 0; z < result.length; z++) {
                ctx.strokeStyle = "#FF2600";
                ctx.lineWidth = 5;
                ctx.strokeRect(result[z]['x'], result[z]['y'], result[z]['width'], result[z]['height']);
                ctx.fillStyle = "#ff2800";
                ctx.font = "700 12px Arial";
                var text = result[z]['sn'] + '.' + result[z]['bdno'];
                ctx.fillText(text, result[z]['x'], result[z]['y'] - 20);
              }

              var newrect = {};
              var lastsn = 0;

              if (result.length > 0) {
                lastsn = result[result.length - 1]["sn"];
              }

              newrect['x'] = rect.startX;
              newrect['y'] = rect.startY;
              newrect['width'] = rect.w;
              newrect['height'] = rect.h;
              newrect['sn'] = lastsn + 1;
              newrect['bdno'] = "";
              newrect['name'] = "";
              newrect['rank'] = "";
              newrect['nameplate'] = "";
              newrect['marked'] = true;
              newrect['rankinpic'] = "";
              newrect['isManush'] = true;
              self.get('newrect').push(newrect);
            }
          }, false);

          _this.get('bus').recogpublish(_this.get('recogArray'));
        }))["catch"]();
      }
    },
    draw: function draw() {
      var arr = this.get('newrect');

      if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
          this.get('recogArray').push(arr[i]);
        }

        var newarrRec = this.get('recogArray');

        var arr1 = _toConsumableArray(new Map(newarrRec.map(function (item) {
          return [item['sn'], item];
        })).values());

        this.set('recogArray', arr1);
        this.get('bus').recogpublish(this.get('recogArray'));
      }
    }
  }
});

exports["default"] = _default;