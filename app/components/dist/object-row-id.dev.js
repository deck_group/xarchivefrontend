"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _objectRowIdValidator = _interopRequireDefault(require("../mixins/object-row-id-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectRowIdValidator["default"], {
  tagName: "td",
  classNames: ["col-sm-1"],
  classInput: "form-control input-txt-table",
  isReadonly: false
});

exports["default"] = _default;