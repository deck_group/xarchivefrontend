"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "tr",
  tdClassOne: "col-sm-7 o_td_label",
  labelClass: "o_form_label",
  tdClassTwo: "col-sm-5 padding-left",
  spanClass: "bigcheck",
  spanLabelClass: "bigcheck",
  inputClass: "bigcheck",
  spanTargetClass: "bigcheck-target",
  actions: {
    optionsChecked: function optionsChecked() {
      this.set('data', document.getElementById("retired").checked);
    }
  }
});

exports["default"] = _default;