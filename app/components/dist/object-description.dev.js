"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _objectDescriptionValidator = _interopRequireDefault(require("../mixins/object-description-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectDescriptionValidator["default"], {
  tagName: 'tr',
  isReadonly: false,
  label: "Object Title",
  tableLabelClass: "col-sm-4 padding-0 o_td_label",
  labelClass: "o_form_label",
  tdClass: "col-sm-7 padding-left",
  inputClass: "form-control textareascroll",
  row: 4,
  flagerror: false,
  minlength: 0,
  maxlength: 16384,
  actions: {
    setDescription: function setDescription() {
      this.set('message', undefined);
      this.set('message', this.validate(this.get('desc'), this.get('label'), this.get('maxlength'), this.get('minlength')));

      if (this.get('message').length > 0) {
        this.set('flagerror', true);
      }

      if (this.get('flagerror') === false) {
        this.attrs.changeDescription(this.get('desc'), this); // this.sendAction('changeDescription', this.get('desc'), this)
      }
    }
  }
});

exports["default"] = _default;