"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _numberField = _interopRequireDefault(require("../mixins/number-field"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_numberField["default"], {
  tagName: "tr",
  classTdOne: "col-sm-7 col-xs-12 col-12 o_td_label",
  classLabel: "o_form_label",
  label: "Object Title",
  classTdTwo: "col-sm-5 col-xs-12 col-12 padding-left",
  classInput: "form-control required",
  isPresence: true,
  message: undefined,
  bus: (0, _service.inject)("pubsub"),
  flagfor: "",
  isReadonly: false,
  isBoardcast: false,
  isAspectRatio: false,
  data: 0,
  format: "^[0-9]*$",
  actions: {
    clearMessage: function clearMessage() {
      this.set('message', undefined);
    },
    checkNumber: function checkNumber() {
      this.set('message', undefined);
      this.set('message', this.validate(this.get('data'), this.get('isPresence'), this.get('label'), this.get('format')));

      if (this.get('message').length > 0) {
        this.set('setDependencyError', true);
      } else {
        this.set('setDependencyError', false);
      }

      if (this.get("isBoardcast") === true) {
        if (this.get("isAspectRatio") === true) {
          this.get('bus').sendImageParameter(this.get('data'), this.get("flagfor"));
        }
      }
    }
  }
});

exports["default"] = _default;