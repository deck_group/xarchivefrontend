"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "td",
  classNames: ["col-sm-4"],
  classSelection: "rankselection",
  store: (0, _service.inject)(),
  keyaccess: "name",
  model: (0, _object.computed)(function () {
    return this.get('store').findAll(this.get('modelname'));
  }),
  actions: {
    selectedOption: function selectedOption(value) {
      this.set('data', value);
    }
  }
});

exports["default"] = _default;