"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "td",
  classNames: ["col-sm-2"],
  classInput: "form-control required-3",
  isReadonly: false,
  placeholder: "",
  store: (0, _service.inject)(),
  keyaccess: "name",
  bus: (0, _service.inject)("pubsub"),
  keystosearch: [],
  needtobroadcast: false,
  newdat: undefined,
  sortitem: "name",
  orderby: "asc",
  model: (0, _object.computed)(function () {
    this.set('newdat', this.get("store").findAll(this.get('modelname')));
    return this.get("store").findAll(this.get('modelname'));
  }),
  sortingByArray: (0, _object.computed)(function () {
    var array = [];
    var data = this.get("sortitem") + ':' + this.get("orderby");
    array.push(data);
    return array;
  }),
  sortedCollection: _object.computed.sort('model', 'sortingByArray'),
  actions: {
    setvalue: function setvalue() {
      var that = this;

      if (this.get("needtobroadcast") === true) {
        this.get("newdat").forEach(function (item) {
          if (item[that.get('keyaccess')] === that.get("data")) {
            that.get('bus').dataPublish(item, that.get('sn'));
          }
        });
      }
    }
  }
});

exports["default"] = _default;