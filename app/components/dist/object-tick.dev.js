"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "tr",
  classTdOne: "col-sm-7 col-xs-12 col-12 o_td_label",
  classLabel: "o_form_label",
  label: "Object Title",
  classTdTwo: "col-sm-5 col-xs-12 col-12 padding-left",
  classInput: "form-control required",
  ticked: false,
  holderflag: undefined,
  isBoardcast: false,
  bus: (0, _service.inject)("pubsub"),
  actions: {
    optionsChecked: function optionsChecked() {
      var flag = false;

      if (document.querySelector('#retired:checked') !== null) {
        flag = true;
      }

      this.set("ticked", flag);

      if (this.get("isBoardcast") === true) {
        if (this.get("holderflag") !== undefined) {
          this.get('bus').sendImageCorrectionOption(flag, this.get("holderflag"));
        } else {
          this.get('bus').sendAspectRatio(flag);
        }
      }
    }
  }
});

exports["default"] = _default;