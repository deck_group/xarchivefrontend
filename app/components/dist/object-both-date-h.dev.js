"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _objectBothDateValidator = _interopRequireDefault(require("../mixins/object-both-date-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectBothDateValidator["default"], {
  tagName: "td",
  classNames: ['col-sm-9'],
  classTd: "padding-tot",
  classInput: "form-control required-3",
  isReadonly: false,
  isPresence: true,
  before: new Date(),
  after: '01/01/1900',
  label1: "Object Title",
  label2: "Object Title",
  message: undefined,
  actions: {
    checkdate: function checkdate() {
      this.set('message', undefined);
      this.set('message', this.validatestart(this.get('modelstartdate'), this.get('isPresence'), this.get('label1'), this.get('after'), this.get('before')));

      if (this.get('message').length > 0) {
        this.set('modelstartdate', '');
      }
    },
    checkstartdate: function checkstartdate() {
      this.set('message', undefined);
      this.set('message', this.validate(this.get('modelstartdate'), this.get('modelenddate'), this.get('isPresence'), this.get('label1'), this.get('label2'), this.get('after'), this.get('before')));

      if (this.get('message').length > 0) {
        this.set('modelenddate', '');
        this.set('modelstartdate', '');
      }
    },
    clearMessage: function clearMessage() {
      this.set('message', undefined);
    }
  }
});

exports["default"] = _default;