"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "tr",
  spanClass: "bigcheck",
  spanLabelClass: "bigcheck",
  inputClass: "bigcheck",
  spanTargetClass: "bigcheck-target",
  autoisReadonly: false,
  titleReadonly: true,
  nameReadonly: true,
  mode: ""
});

exports["default"] = _default;