"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "td",
  classNames: ["col-sm-1"],
  totalarray: [],
  keyAccess: "sn",
  needtobroadcast: false,
  bus: (0, _service.inject)("pubsub"),
  actions: {
    "delete": function _delete(event) {
      var newarray = [];
      var keytoremove = event.parentView.model[this.get('keyAccess')];
      var oldArray = this.get('totalarray');

      for (var i = 0; i < oldArray.length; i++) {
        if (oldArray[i][this.get('keyAccess')] !== keytoremove) {
          newarray.push(oldArray[i]);
        }
      }

      this.set('totalarray', newarray);

      if (this.get('needtobroadcast') === true) {
        this.get('bus').revisedPublish(newarray);
      } else {
        var element_delete = document.getElementById(event.parentView.elementId);
        element_delete.remove();
      }
    }
  }
});

exports["default"] = _default;