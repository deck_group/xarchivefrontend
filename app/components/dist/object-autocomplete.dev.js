"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "div",
  store: (0, _service.inject)(),
  keyaccess: "name",
  classNames: ["row"],
  array: [],
  classInput: "form-control input-txt-table-deep-orange",
  classDivone: "col-sm-2",
  classDivtwo: "col-sm-7 col-xs-7 col-7",
  classTag: "tagstotal",
  classRowTwo: "row margin-top-tab",
  classRow: "row",
  mode: "",
  sortitem: "name",
  orderby: "asc",
  model: (0, _object.computed)(function () {
    return this.get("store").findAll(this.get('modelname'));
  }),
  sortingByArray: (0, _object.computed)(function () {
    var array = [];
    var data = this.get("sortitem") + ':' + this.get("orderby");
    array.push(data);
    return array;
  }),
  sortedCollection: _object.computed.sort('model', 'sortingByArray'),
  arrayComputed: (0, _object.computed)('array', function () {
    return this.get('array');
  }),
  actions: {
    setvalue: function setvalue() {
      var dict = {};
      dict.name = this.get('data');
      this.get('array').pushObject(dict);
    }
  }
});

exports["default"] = _default;