"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var _default = _component["default"].extend({
  tagName: "td",
  spanClass: "bigcheck",
  spanLabelClass: "bigcheck",
  inputClass: "bigcheck",
  spanTargetClass: "bigcheck-target",
  array: [],
  bus: (0, _service.inject)("pubsub"),
  keyaccess: "marked",
  keytomatch: "sn",
  actions: {
    optionsChecked: function optionsChecked() {
      var _this = this;

      var newarray = [];

      for (var i = 0; i < this.get('array').length; i++) {
        if (this.get('sn') === this.get('array')[i][this.get('keytomatch')]) {
          var recogdict = {};
          Object.entries(this.get('array')[i]).forEach(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                key = _ref2[0],
                value = _ref2[1];

            if (key === _this.get('keyaccess')) {
              console.log(_this.get('array')[i][key]);

              if (_this.get('array')[i][key] === true || _this.get('array')[i][key] === "True") {
                recogdict[key] = false;

                _this.set('data', false);
              } else {
                recogdict[key] = true;

                _this.set('data', true);
              }
            } else {
              recogdict[key] = value;
            }
          });
          newarray.push(recogdict);
        } else {
          newarray.push(this.get('array')[i]);
        }
      }

      this.get('bus').markedPublish(newarray);
    }
  }
});

exports["default"] = _default;