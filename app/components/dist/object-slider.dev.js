"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  showErrors: true,
  tagName: 'tr',
  isReadonly: false,
  message: undefined,
  flagerror: false,
  format: "^[a-zA-Z0-9\\s.@-_']*$",
  isPresence: true,
  leasdingspace: true,
  label: "Object Title",
  tableLabelClass: "col-sm-4 padding-0 o_td_label",
  labelClass: "o_form_label",
  tdClass: "col-sm-7 padding-left",
  inputClass: "cursor-slider",
  minlength: -100,
  maxlength: 100,
  isBoardcast: false,
  holderflag: undefined,
  type: "",
  step: 1,
  bus: (0, _service.inject)("pubsub"),
  data: 0,
  mode: "",
  actions: {
    setValue: function setValue(data) {
      if (this.get("isBoardcast") === true) {
        if (this.get("holderflag") !== undefined) {
          this.get('bus').sendImageCorrectionOption(data, this.get("holderflag"));
        }
      }
    }
  }
});

exports["default"] = _default;