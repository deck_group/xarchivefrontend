"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  store: (0, _service.inject)(),
  objEvent: undefined,
  objDt: undefined,
  objDesc: undefined,
  newmodel: {},
  actions: {
    setTitle: function setTitle(value, obj) {
      this.set('objEvent', obj);
      this.set('newmodel.name', value);
    },
    setDate: function setDate(value, obj) {
      this.set('objDt', obj);
      this.set('newmodel.date', value);
    },
    setDescription: function setDescription(value, obj) {
      this.set('objDesc', obj);
      this.set('newmodel.description', value);
    },
    save: function save() {
      var newevent = this.get("store").createRecord("event");
      newevent.set("name", this.get('newmodel.name'));
      newevent.set("date", this.get('newmodel.date'));
      newevent.set("description", this.get('newmodel.description'));

      if (this.get('objEvent') !== undefined || this.get('objDt') !== undefined) {
        if (this.get('objDesc') === undefined) {
          newevent.set("description", 'N/A');
          this.send('saveModel', newevent);
        } else {
          if (this.get('objDesc').get('flagerror') === false) {
            if (newevent.get("description") == undefined) {
              newevent.set("description", 'N/A');
            }

            this.send('saveModel', newevent);
          }
        }
      }
    },
    saveModel: function saveModel(newevent) {
      var _this = this;

      newevent.save().then(function () {
        _this.get('objEvent').set('title', undefined);

        _this.get('objDt').set('dt', '');

        if (_this.get('objDesc') !== undefined) {
          _this.get('objDesc').set('desc', undefined);
        }

        _this.set('newmodel', {});

        var ok = document.getElementById('discardEvent');
        ok.addEventListener("click", _this.send("modalclose"));
      });
    },
    modalclose: function modalclose() {
      var modal = document.getElementById('eventNew');
      this.get('tasksController').send("callEvent");
      modal.style.display = 'none';
    }
  }
});

exports["default"] = _default;