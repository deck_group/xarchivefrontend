"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberCpValidations = require("ember-cp-validations");

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = (0, _emberCpValidations.buildValidations)({
  title: [(0, _emberCpValidations.validator)('presence', {
    presence: true,
    message: 'Event name should not be empty'
  })]
});

exports["default"] = _default;