import Component from '@ember/component';
import dateFieldValidator from '../mixins/object-both-date-validator';

export default Component.extend(dateFieldValidator, {
    tagName: `td`,
    classNames: ['col-sm-9'],
    classTd: `padding-tot`,
    classInput: `form-control required-3`,
    isReadonly: false,
    isPresence: true,
    before: new Date(),
    after: '01/01/1900',
    label1: `Object Title`,
    label2: `Object Title`,
    message: undefined,
    actions: {
        checkdate() {
            this.set('message', undefined)
            this.set('message', this.validatestart(this.get('modelstartdate'), this.get('isPresence'), this.get('label1'), this.get('after'), this.get('before')))
            if (this.get('message').length > 0) {
                this.set('modelstartdate', '')
            }
        },
        checkstartdate() {
            this.set('message', undefined)
            this.set('message', this.validate(this.get('modelstartdate'), this.get('modelenddate'), this.get('isPresence'), this.get('label1'), this.get('label2'), this.get('after'), this.get('before')))
            if (this.get('message').length > 0) {
                this.set('modelenddate', '')
                this.set('modelstartdate', '')
            }

        },
        clearMessage() {
            this.set('message', undefined)
        },

    }
});
