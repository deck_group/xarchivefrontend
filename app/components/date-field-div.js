import Component from '@ember/component';


export default Component.extend({
    tagName: 'div',
    dt:undefined,
    classNames:[`col-sm-6 col-6 col-xs-6`],
    fieldtype: 'date',
    label: "Object Title",
    tableLabelClass: "col-sm-6 padding-0 o_td_label_one",
    labelClass: "o_form_label-2",
    tdClass: "col-sm-5 padding-left",
    inputClass: "form-control required-30",
 
});
