import Component from '@ember/component';
import numberFieldValidator from '../mixins/number-field';
import { inject as service } from '@ember/service';

export default Component.extend(numberFieldValidator, {
    tagName: `tr`,
    classTdOne: `col-sm-7 col-xs-12 col-12 o_td_label`,
    classLabel: `o_form_label`,
    label: `Object Title`,
    classTdTwo: `col-sm-5 col-xs-12 col-12 padding-left`,
    classInput: `form-control required`,
    isPresence: true,
    message: undefined,
    bus: service("pubsub"),
    flagfor:``,
    isReadonly: false,
    isBoardcast: false,
    isAspectRatio: false,
    data: 0,
    format: `^[0-9]*$`,
    actions: {
        clearMessage() {
            this.set('message', undefined)
        },

        checkNumber() {
            this.set('message', undefined)
            this.set('message',
                this.validate(this.get('data'),
                    this.get('isPresence'),
                    this.get('label'),
                    this.get('format')
                )
                )

            if (this.get('message').length > 0) {
                this.set('setDependencyError', true)

            }
            else {
                this.set('setDependencyError', false)
            }
            if (this.get("isBoardcast") === true) {
                if (this.get("isAspectRatio") === true) {
                    this.get('bus').sendImageParameter(this.get('data'),this.get("flagfor"));
                }

            }

        }
    }
});
