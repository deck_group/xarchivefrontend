import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    store: service(),
    keyaccess: `name`,
    classNames: [`row`],
    array:[],
    tableLabelClass: "o_td_label",
    classInput:`form-control input-txt-table-deep-orange`,
    classDivone:`col-sm-2`,
    classDivtwo:`col-sm-7 col-xs-7 col-7`,
    classTag:`tagstotal`,
    classRowTwo:`row margin-top-tab`,
    classRow:`row`,
    mode:``,
    sortitem: `name`,
    orderby: `asc`,
    model: computed(function () {
        return this.get("store").findAll(this.get('modelname'))
    }),
    sortingByArray: computed(function () {
        var array = []
        var data = this.get("sortitem") + ':' + this.get("orderby")
        array.push(data)
        return array
    }),
    sortedCollection: computed.sort('model', 'sortingByArray'),
    arrayComputed:computed('array', function () {
        return this.get('array')
    }),

    actions:{
        setvalue() {
            var dict = {}
            dict.name=this.get('data')
            this.get('array').pushObject(dict);
            this.set("data",undefined)
        }
    }
});
