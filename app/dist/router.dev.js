"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _router = _interopRequireDefault(require("@ember/routing/router"));

var _environment = _interopRequireDefault(require("./config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Router = _router["default"].extend({
  location: _environment["default"].locationType,
  rootURL: _environment["default"].rootURL
});

Router.map(function () {
  this.route('login', {
    path: '/'
  });
  this.route('personnels');
  this.route('personnel-new', {
    path: '/personnel/new'
  });
  this.route('personnel-view', {
    path: '/personnel/:personnel_id/show'
  });
  this.route('personnel-edit', {
    path: '/personnel/:personnel_id/edit'
  });
  this.route('asset');
  this.route('asset-new', {
    path: '/asset/new'
  });
  this.route('aircrafts');
  this.route('aircraft-new', {
    path: '/aircraft/new'
  });
  this.route('aircraft-edit', {
    path: '/aircraft/:aircraft_id/edit'
  });
  this.route('aircraft-show', {
    path: '/aircraft/:aircraft_id/show'
  });
  this.route('asset-view', {
    path: '/asset/:asset_id/show'
  });
  this.route('asset-edit', {
    path: '/asset/:asset_id/edit'
  });
  this.route('edit-image');
  this.route('imageeditor');
});
var _default = Router;
exports["default"] = _default;