import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        createroute() {
            this.transitionToRoute("personnel-new")
        },
        detail(id){
            this.transitionToRoute('personnel-view',id)
        }

        
    }
});
