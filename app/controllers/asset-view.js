import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        discardroute() {
            this.transitionToRoute("asset")
        },
        edit(id) {
            this.transitionToRoute('asset-edit', id)
        },
        commentTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            comments.setAttribute("class", "active")
            comments.setAttribute("class", "show")

            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane active show")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")


            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        otherTagsTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")
            other.setAttribute("class", "active")
            other.setAttribute("class", "show")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane active show")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        studioTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")
            studio.setAttribute("class", "active")
            studio.setAttribute("class", "show")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane active show")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        gallery() {

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")
            gal.setAttribute("class", "active")
            gal.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane active show")
        },


        events() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            event.setAttribute("class", "active")
            event.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane active show")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        location() {
            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")
            location.setAttribute("class", "active")
            location.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane active show")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")

        },
        personnel() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")
            personnel.setAttribute("class", "active")
            personnel.setAttribute("class", "show")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane active show")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        aircraft() {
            var air = document.getElementById("air")
            air.setAttribute("class", "nav-link")
            air.setAttribute("class", "active")
            air.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")

            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane active show")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")



        },


    }
});
