import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    newmodel: {},
    store: service(),
    actions: {
        submit(newmodel) {
            var that = this
            if (Object.keys(newmodel).length === 0) {
                localStorage.setItem('token', undefined)

            }
            else {
                this.get("store").query("logauth", {
                    query: {
                        value1: newmodel.loginid,
                        value: newmodel.password
                    }
                }).then(
                    function (model) {
                        if (model.content.length === 0) {
                            that.send("errormsgdef", "Please check the Login ID or Password")
                        }
                        else {
                            model.forEach(function (data) {
                                that.set("newmodel", {})
                                localStorage.setItem('token', data.token)
                                localStorage.setItem('user', data.loginid)
                                localStorage.setItem('role', data.role)
                                that.transitionToRoute('personnels')
                            });
                        }

                    }
                )


            }
        },
        errormsgdef(msg) {
            this.get('flashMessages').danger(msg, {
                timeout: 3000,
                priority: 100,
                sticky: false,
                showProgress: true
            })
        }
    }

});
