import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';


export default Controller.extend({
    store: service(),
    sn: 0,
    ranks: computed(function () {
        return this.get("store").findAll("rank")
    }),

    actions: {
        discardroute(id) {
            this.transitionToRoute('personnel-view', id)
        },
        save(personnel, id, status) {
            personnel.set('status', status)
            var rankhistarray = []
            var elements = document.getElementById('personrow').getElementsByTagName('tr');
            for (var p = 0; p < elements.length; p++) {
                var ele_id = (elements[p].id)
                var dict = {}
                var sn_id = "sn_" + ele_id;
                var rank_id = "rank_" + ele_id;
                var startdate_id = "startdate_" + ele_id;
                var enddate_id = "enddate_" + ele_id;
                var sn = document.getElementById(sn_id).value;
                var rank = document.getElementById(rank_id).value;
                var startdate = document.getElementById(startdate_id).value;
                var enddate = document.getElementById(enddate_id).value
                dict['sn'] = sn
                dict['rank'] = rank
                dict['startdate'] = startdate
                dict['enddate'] = enddate
                rankhistarray.push(dict)
                if (personnel.get("bdno") === undefined || personnel.get("currentrank") === undefined || personnel.get("bdno") === "" || personnel.get("currentrank") === "") {
                    var msg = " - "
                    if (personnel.get("bdno") === undefined || personnel.get("bdno") === "") {
                        msg = msg + ' Please provide the BDNO. -'
                    }
                    if (personnel.get("currentrank") === undefined || personnel.get("currentrank") === "") {
                        msg = msg + ' Please provide the current rank -'
                    }
                    this.send('errormsgdef', msg)
                } else {
                    personnel.set("rankhistory", undefined)
                    personnel.set("rankhistory", rankhistarray)
                    personnel.save().then(() => {
                        this.transitionToRoute("personnel-view", id)
                    })
                }

            }

        },

        retiredofficer() {
            this.set("personnel.isretired", document.getElementById("retired").checked)

        },

        addEventColumn() {
            var element_id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            var row = document.createElement("tr");
            row.setAttribute("id", element_id);
            var element = document.getElementById("personrow");
            var elementsPersonal = document.getElementById("personrow").getElementsByTagName('tr');
            var snarray = []
            if (elementsPersonal.length === 0) {
                snarray.push(0)
            }
            else {
                for (var p = 0; p < elementsPersonal.length; p++) {
                    var id = (elementsPersonal[p].id)
                    var sn = 'sn_' + id
                    var snValue = document.getElementById(sn).value
                    if (snValue === 'N/A') {
                        snarray.push(0)
                    }
                    else {
                        snarray.push(snValue)
                    }

                }
            }


            this.set("sn", 1 + parseInt(snarray.pop()));
            var snareadiv = document.createElement("td");
            snareadiv.setAttribute("class", "col-sm-1");
            var sn_id = 'sn_' + element_id
            var sninput = document.createElement("input");
            sninput.setAttribute("class", "form-control");
            sninput.setAttribute("id", sn_id);
            sninput.setAttribute("class", "input-txt-table");
            sninput.setAttribute("disabled", true);
            sninput.value = this.get('sn');
            snareadiv.append(sninput);




            var startdateTD = document.createElement("td");
            var startdate_id = 'startdate_' + element_id

            startdateTD.setAttribute("class", "col-sm-4");
            var startdate = document.createElement("input");
            startdate.setAttribute("id", startdate_id);
            startdate.setAttribute("type", "date");
            startdate.setAttribute("class", "form-control required-3");
            startdateTD.append(startdate);

            var enddateTD = document.createElement("td");
            var enddate_id = 'enddate_' + element_id
            enddateTD.setAttribute("class", "col-sm-4");
            var enddate = document.createElement("input");
            enddate.setAttribute("id", enddate_id);
            enddate.setAttribute("type", "date");
            enddate.setAttribute("class", "form-control required-3");
            enddateTD.append(enddate);


            var deleteTD = document.createElement("td");
            deleteTD.setAttribute("class", "col-sm-1");
            var btn = document.createElement("BUTTON");
            btn.setAttribute("class", "btn btn-primary-deep-orange btn1");
            var bt_id = 'btn_' + element_id
            btn.setAttribute("id", bt_id);
            var icon = document.createElement("span");
            icon.setAttribute("class", "fa fa-times")
            btn.append(icon)
            btn.addEventListener('click', function () {
                var element_delete = document.getElementById(element_id);
                element_delete.remove();
            }, false);
            deleteTD.append(btn);

            var rankid='rank_'+element_id
            var datalistid='ranks_'+element_id
            var rankinpicTD = document.createElement("td");
            rankinpicTD.setAttribute("class", "col-sm-4");
            var rank = document.createElement("input");
            rank.setAttribute("id", rankid);
            rank.setAttribute("class", "form-control required");
            rank.setAttribute("placeholder", "Select rank");
            rank.setAttribute("data-min-length", "1");
            rank.setAttribute("multiple", "");
            rank.setAttribute("data-selection-required", "1");
            rank.setAttribute("list", datalistid);
            rank.setAttribute("name", "rank22");
            var datalist_rank = document.createElement("datalist");

            datalist_rank.setAttribute("id", datalistid);
            this.get("ranks").then(function (model) {
                var z = document.createElement("option");
                z.setAttribute("value", "");
                var t = document.createTextNode("");
                z.appendChild(t);
                datalist_rank.appendChild(z);
                model.forEach(function (data) {
                    var z = document.createElement("option");
                    z.setAttribute("value", data.name);
                    var t = document.createTextNode(data.name);
                    z.appendChild(t);
                    datalist_rank.appendChild(z);
                })

            })
            rank.append(datalist_rank)
            rankinpicTD.append(rank);

            row.append(snareadiv);
            row.append(rankinpicTD);
            row.append(startdateTD);
            row.append(enddateTD);
            row.append(deleteTD);
            element.appendChild(row);



        },
        deleterow(id) {
            document.getElementById(id).remove()
        },
        errormsgdef(msg) {
            var modal = document.getElementById('msg');
            var ok = document.getElementById('discardmsg');
            ok.onclick = function () {
                modal.style.display = 'none';
            }
            modal.style.display = 'block';
            document.getElementById('message').innerHTML = msg

        }
    }

});
