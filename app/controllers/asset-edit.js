import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
    ifimageEdited: false,
    bus: service("pubsub"),
    storage: service("storage"),
    init() {
        this._super();
        this.get('bus').on('imagedatanew', this, this.recivedImage);
        this.get('bus').on('recognize', this, this.recivedDataForPersonnel);
        this.get('bus').on('asset', this, this.senddata);
    },
    urls: service("service"),
    personnelCompute: computed('personnals', function () {
        return this.get("asset").get("personnel")
    }),

    commentcompute: computed('comments', function () {
        return this.get('asset').comment
    }),

    aircraftCompute: computed('aircrafts', function () {
        return this.get('asset').aircraft
    }),

    senddata() {
        this.get('bus').sendDataPublish(this.get("asset").get("personnel"));
    },
    recivedDataForPersonnel(data) {
        this.get("asset").set("personnel", undefined)
        this.get("asset").set("personnel", data)
        this.get('personnelCompute')
    },

    imageCompute: computed('image', function () {
        return this.get('image')
    }),

    recivedImage(data) {
        if (data == true) {
            this.get("asset").set("imagedataarray", this.get("storage").imagestorage)
            this.get("imageCompute")
            this.set("ifimageEdited", data)
            this.get("ifimageEditedComputed")
        }

    },

    actions: {
        discardroute(id) {
            this.transitionToRoute('asset-view', id)
        },
        addEventColumn() {
            var dict = {}
            var sn = ((this.get('asset').comment).length)
            dict['sn'] = sn + 1
            dict.commentFrom = localStorage.getItem("user")
            dict.dt = moment(new Date()).format("YYYY-MM-DD")
            dict.comment = ``
            this.get('asset').comment.pushObject(dict)
            this.get('commentcompute')
        },
        save(model, routeid, status) {
            document.getElementById("overlay").style.display = "block";
            model.set('isrecognised', 1)
            model.set("status", status)
            model.save().then(() => {
                document.getElementById("overlay").style.display = "none";
                this.transitionToRoute("asset-view", routeid)
                
            })
            .catch((error) => {
                document.getElementById("overlay").style.display = "none";
                this.get('flashMessages').danger(error, { timeout: 3000 })
            }).finally()

        },

        commentTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            comments.setAttribute("class", "active")
            comments.setAttribute("class", "show")

            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane active show")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")


            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        otherTagsTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")
            other.setAttribute("class", "active")
            other.setAttribute("class", "show")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane active show")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        studioTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")
            studio.setAttribute("class", "active")
            studio.setAttribute("class", "show")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane active show")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        gallery() {

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")
            gal.setAttribute("class", "active")
            gal.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane active show")
        },


        events() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            event.setAttribute("class", "active")
            event.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane active show")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        location() {
            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")
            location.setAttribute("class", "active")
            location.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane active show")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")

        },
        personnel() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")
            personnel.setAttribute("class", "active")
            personnel.setAttribute("class", "show")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane active show")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        aircraft() {
            var air = document.getElementById("air")
            air.setAttribute("class", "nav-link")
            air.setAttribute("class", "active")
            air.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")

            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane active show")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")



        },

        addaircraft() {
            var dict = {}
            var sn = ((this.get('asset').aircraft).length)
            dict['sn'] = sn + 1
            dict.name = ``
            this.get('asset').aircraft.pushObject(dict)
            this.get('aircraftCompute')
        },


    }
});
