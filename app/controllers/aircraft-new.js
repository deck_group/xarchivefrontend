import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
    newmodel: {},
    urls: service("service"),
    sn: 1,
    imagearray: [],
    image: undefined,
    image_id: 1,
    imageurl: [],
    imgGallery: [],
    store: service(),
    imageurlArray: computed('imagearray', function () {
        return this.get('imagearray')
    }),

    actions: {
        save(newmodel, aircraftid) {
            console.log(aircraftid)
            let newaircraft = this.get("store").createRecord("aircraft");
            newaircraft.set("aircraftid", aircraftid)
            newaircraft.set("name", newmodel.name)
            newaircraft.set("description", newmodel.description)
            newaircraft.set("status", 'draft')
            newaircraft.set('imagedataarray', this.get('imagearray'))
            if (newaircraft.get("name") !== undefined && newaircraft.get("imagedataarray").length > 0) {
                if (newaircraft.get("description") === undefined || newaircraft.get("description") === "") {
                    newaircraft.set("description", 'N/A')
                }
                for (var j = 0; j < newaircraft.get('imagedataarray').length; j++) {
                    var imagename='aircraft-'+newaircraft.get("aircraftid")+"-"+newaircraft.get('imagedataarray')[j]['imagename']
                    var imgedict = {}
                    var imageurl = this.get("urls.imageurl") + '/static'
                    newaircraft.get('imagedataarray')[j]['imagename']=imagename
                    imgedict['url'] = imageurl + '/' + imagename
                    this.get('imageurl').push(imgedict)
                }
                newaircraft.set('imageurls', this.get('imageurl'))
                newaircraft.save().then(() => {
                    this.set("imgGallery", [])
                    this.set("imageData", undefined)
                    this.set("image", undefined)
                    this.set("name", undefined)
                    this.set("newmodel", {})
                    this.transitionToRoute("aircrafts")
                }).catch((error) => {

                    this.get('flashMessages').danger(error, { timeout: 3000 })
                }).finally()
            }
            else {
                this.get('flashMessages').danger('Check the fields', { timeout: 3000 })

            }

        },

        discardroute() {
            this.set("newmodel", {})
            this.set("imgGallery", [])
            this.set("image", undefined)
            this.set("name", undefined)
            this.set("imageData", undefined)
            this.transitionToRoute("aircrafts")
        },

    }
});
