import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        createroute() {
            this.transitionToRoute("aircraft-new")
        },
        detail(id){
            this.transitionToRoute('aircraft-show',id)
        }

        
    }
});