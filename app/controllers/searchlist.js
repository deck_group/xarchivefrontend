import Controller from '@ember/controller';
import { inject as service } from '@ember/service';


export default Controller.extend({
    urls: service("service"),
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,

    actions: {
        discardroute() {
            this.set("data", undefined)
            this.transitionToRoute("search")
        },
        detail(id) {
            this.transitionToRoute('asset-view', id)
        },
        search(data) {
            const store = this.get('store');
            this.set("searches", store.query('search', {
                filter: data,
                page: {
                    number: 1,
                    size: 10

                }
            }))
        }
    }
});
