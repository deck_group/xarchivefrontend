import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        discardroute() {
            this.transitionToRoute('personnels')
        },
        edit(id) {
            this.transitionToRoute('personnel-edit', id)
        }

    }
});
