"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  newmodel: {
    'bdno': "",
    'name': "",
    'currentrank': "",
    'isretired': false
  },
  urls: (0, _service.inject)("service"),
  sn: 1,
  imagearray: [],
  rankhistory: {},
  imageurl: [],
  componentNames: [],
  componentNamesCom: (0, _object.computed)('componentNames', function () {
    return this.get('componentNames');
  }),
  ranks: (0, _object.computed)(function () {
    return this.get("store").findAll("rank");
  }),
  actions: {
    personnelroute: function personnelroute() {
      this.set("newmodel", {});
      this.set("imgGallery", []);
      this.set("componentNames", []);
      this.transitionToRoute("personnels");
    },
    save: function save(status) {
      var _this = this;

      var imagearray = [];
      var newpersonnel = this.get("store").createRecord("personnel");

      if (this.get('componentNames').length === 0) {
        var array = [];
        var dict = {};
        dict['sn'] = 0;
        dict.rank = "";
        dict.startdate = "";
        dict.enddate = "";
        array.pushObject(dict);
        newpersonnel.set("rankhistory", array);
      } else {
        newpersonnel.set("rankhistory", this.get('componentNames'));
      }

      newpersonnel.set("isretired", this.get('newmodel').isretired);

      if (this.get('imagearray').length > 0 && this.get('newmodel').bdno !== "" && this.get('newmodel').name !== "" && this.get('newmodel').currentrank !== "") {
        newpersonnel.set("bdno", this.get('newmodel').bdno);
        newpersonnel.set("name", this.get('newmodel').name);
        newpersonnel.set("currentrank", this.get('newmodel').currentrank);
        newpersonnel.set('imagedataarray', this.get('imagearray'));

        for (var j = 0; j < newpersonnel.get('imagedataarray').length; j++) {
          var imgedict = {};
          var imageurl = this.get("urls.imageurl") + '/static/images/personnel/';
          imgedict['url'] = imageurl + this.get('newmodel').bdno + '/' + newpersonnel.get('imagedataarray')[j]['imagename'];
          this.get('imageurl').push(imgedict);
          var newdict = {};
          newdict['bdno'] = this.get('newmodel').bdno;
          newdict['imagename'] = newpersonnel.get('imagedataarray')[j]['imagename'];
          newdict['imagedirectory'] = imageurl + this.get('newmodel').bdno + '/' + newpersonnel.get('imagedataarray')[j]['imagename'];
          imagearray.push(newdict);
        }

        newpersonnel.set('imageurls', this.get('imageurl'));
        newpersonnel.set("status", status);
        newpersonnel.set("nameplate", this.get('newmodel').nameplate);

        for (var x = 0; x < imagearray.length; x++) {
          var newimage = this.get("store").createRecord("imagereference");
          newimage.set('bdno', imagearray[x]['bdno']);
          newimage.set('imagename', imagearray[x]['imagename']);
          newimage.set('imagedirectory', imagearray[x]['imagedirectory']);
          newimage.save().then(function () {})["catch"](function (error) {
            _this.get('flashMessages').danger(error, {
              timeout: 3000
            });
          })["finally"]({});
        }

        newpersonnel.save().then(function () {
          _this.set("newmodel", {});

          _this.set("imgGallery", []);

          _this.set("componentNames", []);

          _this.transitionToRoute('personnels');
        })["catch"](function (error) {
          _this.get('flashMessages').danger(error, {
            timeout: 3000
          });
        })["finally"]({});
      } else {
        if (this.get('imagearray').length === 0) {
          this.get('flashMessages').danger('Person should have atleast one image', {
            timeout: 3000
          });
        }

        this.get('flashMessages').danger('Check the fields', {
          timeout: 3000
        });
      }
    },
    discardroute: function discardroute() {
      this.set("newmodel", {});
      this.set("imgGallery", []);
      this.set("componentNames", []);
      this.transitionToRoute("personnels");
    },
    addEventColumn: function addEventColumn() {
      var dict = {};
      dict['sn'] = this.get('sn');
      dict.rank = "";
      dict.startdate = "";
      dict.enddate = "";
      this.set('sn', this.get('sn') + 1);
      this.get('componentNames').pushObject(dict);
    }
  }
});

exports["default"] = _default;