"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  storage: (0, _service.inject)("storage"),
  isAspectRatio: true,
  width: 0,
  height: 0,
  isGrey: false,
  imageComponent: (0, _object.computed)('image', function () {
    return this.get('image');
  }),
  heightComponent: (0, _object.computed)('height', function () {
    return this.get('height');
  }),
  widthComponent: (0, _object.computed)('width', function () {
    return this.get('width');
  }),
  oriheightComponent: (0, _object.computed)('originalheight', function () {
    return this.get('originalheight');
  }),
  oriwidthComponent: (0, _object.computed)('originalwidth', function () {
    return this.get('originalwidth');
  }),
  invertedComponent: (0, _object.computed)('isInverted', function () {
    return this.get('isInverted');
  }),
  aspectComponent: (0, _object.computed)('width', function () {
    return this.get('isAspectRatio');
  }),
  truncateColor: function truncateColor(value) {
    if (value < 0) {
      value = 0;
    } else if (value > 255) {
      value = 255;
    }

    return value;
  },
  init: function init() {
    this._super();

    this.get('bus').on('imageparameter', this, this.recivedImageParameter);
    this.get('bus').on('aspectratio', this, this.recivedAspectRatio);
    this.get('bus').on('imageCorrectionOption', this, this.recivedImageCorrectionOption);
  },
  recivedImageCorrectionOption: function recivedImageCorrectionOption(data, fl) {
    if (fl === "invert") {
      this.send("invertImage", data);
    } else if (fl === "brightness") {
      this.send("fnbrightness", data);
    } else if (fl === "contrast") {
      this.send("fncontrast", data);
    } else if (fl === "grey") {
      this.send("greyImage", data);
    } else if (fl === "hue") {
      this.send("fnhue", data);
    } else if (fl === "saturation") {
      this.send("fnsaturation", data);
    } else if (fl === "lightness") {
      this.send("fnlightness", data);
    }
  },
  recivedAspectRatio: function recivedAspectRatio(data) {
    this.set("isAspectRatio", data);
    this.get("aspectComponent");
  },
  recivedImageParameter: function recivedImageParameter(data, fl) {
    this.set("originalheight", this.get("storage").height);
    this.set("originalwidth", this.get("storage").width);
    this.get("oriheightComponent");
    this.get("oriwidthComponent");

    if (fl === "height") {
      var newh = this.get("storage").height / this.get("storage").width * data;
      this.set("height", parseInt(newh));
      this.get("heightComponent");
    } else {
      var newWidth = this.get("storage").width / this.get("storage").height * data;
      this.set("width", parseInt(newWidth));
      this.get("widthComponent");
    }
  },
  actions: {
    greyImage: function greyImage(fl) {
      var img = this.get("image");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      for (var i = 0; i < imageData.data.length; i += 4) {
        var avg = (imageData.data[i] + imageData.data[i + 1] + imageData.data[i + 2]) / 3;
        imageData.data[i] = avg;
        imageData.data[i + 1] = avg;
        imageData.data[i + 2] = avg;
      }

      context.putImageData(imageData, 0, 0);
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.set('image', resizedImage);
      this.set("oriimage", resizedImage);
    },
    fnlightness: function fnlightness(data) {
      var img = this.get("oriimage");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      context.globalCompositeOperation = data < 100 ? "color-burn" : "color-dodge";
      var colr = 'hsl(' + this.get("storage").hue + ',' + this.get("storage").saturation + '%,' + data + '%)';
      context.fillStyle = colr;
      context.fillRect(0, 0, image.width, image.height);
      context.globalCompositeOperation = "source-over";
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.get("storage").set("lighness", data);
      this.set('image', resizedImage);
    },
    fnhue: function fnhue(data) {
      var img = this.get("oriimage");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      context.globalCompositeOperation = 'hue';
      var colr = 'hsl(' + data + ',' + this.get("storage").saturation + '%,' + this.get("storage").lighness + '%)';
      console.log(colr);
      context.fillStyle = colr;
      context.fillRect(0, 0, image.width, image.height);
      context.globalCompositeOperation = "source-over";
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.get("storage").set("hue", data);
      this.set('image', resizedImage);
    },
    fnsaturation: function fnsaturation(data) {
      var img = this.get("oriimage");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      context.globalCompositeOperation = 'saturation';
      var colr = 'hsl(' + this.get("storage").hue + ',' + data + '%,' + this.get("storage").lighness + '%)';
      context.fillStyle = colr;
      context.fillRect(0, 0, image.width, image.height);
      context.globalCompositeOperation = "source-over";
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.get("storage").set("saturation", data);
      this.set('image', resizedImage);
    },
    fncontrast: function fncontrast(data) {
      var contrast = parseInt(data, 10);
      var factor = 259.0 * (contrast + 255.0) / (255.0 * (259.0 - contrast));
      var img = this.get("oriimage");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      for (var i = 0; i < imageData.data.length; i += 4) {
        imageData.data[i] = this.truncateColor(factor * (imageData.data[i] - 128.0) + 128.0);
        imageData.data[i + 1] = this.truncateColor(factor * (imageData.data[i + 1] - 128.0) + 128.0);
        imageData.data[i + 2] = this.truncateColor(factor * (imageData.data[i + 2] - 128.0) + 128.0);
      }

      context.putImageData(imageData, 0, 0);
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.set('image', resizedImage);
      this.get("storage").set("contrast", contrast);
    },
    fnbrightness: function fnbrightness(data) {
      var brightness = parseInt(data, 10);
      var img = this.get("oriimage");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      for (var i = 0; i < imageData.data.length; i += 4) {
        imageData.data[i] += 255 * (brightness / 100);
        imageData.data[i + 1] += 255 * (brightness / 100);
        imageData.data[i + 2] += 255 * (brightness / 100);
      }

      context.putImageData(imageData, 0, 0);
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.set('image', resizedImage);
      this.get("storage").set("brigthness", brightness);
    },
    invertImage: function invertImage(fl) {
      var img = this.get("image");
      var image = new Image();
      image.src = img;
      var canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;
      var context = canvas.getContext('2d');
      context.drawImage(image, 0, 0, canvas.width, canvas.height);
      var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      for (var i = 0; i < imageData.data.length; i += 4) {
        imageData.data[i] = imageData.data[i] ^ 255;
        imageData.data[i + 1] = imageData.data[i + 1] ^ 255;
        imageData.data[i + 2] = imageData.data[i + 2] ^ 255;
      }

      context.putImageData(imageData, 0, 0);
      var resizedImage = canvas.toDataURL('image/jpeg');
      this.set('image', resizedImage);
      this.set("oriimage", resizedImage);
    },
    resize: function resize() {
      var img = this.get("image");
      var image = new Image();
      image.src = img;
      var resizedImage;
      var canvas = document.createElement('canvas');
      canvas.width = this.get("width");
      canvas.height = this.get("height");
      canvas.getContext('2d').drawImage(image, 0, 0, this.get("width"), this.get("height"));
      resizedImage = canvas.toDataURL('image/jpeg');
      this.set('image', resizedImage);
      this.set("oriimage", resizedImage);
    },
    discardroute: function discardroute() {
      this.get('storage').set("imagestorage", undefined);

      if (this.get('storage').parameter === undefined) {
        this.transitionToRoute(this.get('storage').route);
      }
    },
    save: function save() {
      this.get('storage').set("imagestorage", this.get("image"));
      this.set("width", 0);
      this.set("height", 0);
      this.get('bus').sendImageDataPublish(true);

      if (this.get('storage').parameter === undefined) {
        this.transitionToRoute(this.get('storage').route);
      }
    }
  }
});

exports["default"] = _default;