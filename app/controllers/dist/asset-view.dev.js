"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute("asset");
    },
    edit: function edit(id) {
      this.transitionToRoute('asset-edit', id);
    },
    personnel: function personnel() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      personnel.setAttribute("class", "active");
      personnel.setAttribute("class", "show");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane active show");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    },
    aircraft: function aircraft() {
      var air = document.getElementById("air");
      air.setAttribute("class", "nav-link");
      air.setAttribute("class", "active");
      air.setAttribute("class", "show");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane active show");
    },
    events: function events() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      event.setAttribute("class", "active");
      event.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane active show");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    },
    location: function location() {
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      location.setAttribute("class", "active");
      location.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane active show");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    }
  }
});

exports["default"] = _default;