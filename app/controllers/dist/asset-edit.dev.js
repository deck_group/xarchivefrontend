"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  init: function init() {
    this._super();

    this.get('bus').on('recognize', this, this.recivedDataForPersonnel);
    this.get('bus').on('asset', this, this.senddata);
  },
  urls: (0, _service.inject)("service"),
  personnelCompute: (0, _object.computed)('personnals', function () {
    return this.get("asset").get("personnel");
  }),
  aircraftCompute: (0, _object.computed)('aircrafts', function () {
    return this.get('asset').aircraft;
  }),
  senddata: function senddata() {
    this.get('bus').sendDataPublish(this.get("asset").get("personnel"));
  },
  recivedDataForPersonnel: function recivedDataForPersonnel(data) {
    this.get("asset").set("personnel", undefined);
    this.get("asset").set("personnel", data);
    this.get('personnelCompute');
  },
  actions: {
    discardroute: function discardroute(id) {
      this.transitionToRoute('asset-view', id);
    },
    save: function save(model, routeid, status) {
      var _this = this;

      var last = Math.random().toString(36).substr(2, 1000000000000);
      var imagename = model.assetid + "_" + last + ".jpg";
      var imageurl = this.get("urls.imageurl") + "/static/images/asset/" + model.assetid + "/" + imagename;
      model.set('imageurls', imageurl);
      model.set("status", status);
      model.set('imagename', imagename);
      model.save().then(function () {
        _this.transitionToRoute("asset-view", routeid);
      })["catch"](function (error) {
        _this.get('flashMessages').danger(error, {
          timeout: 3000
        });
      })["finally"]();
    },
    personnel: function personnel() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      personnel.setAttribute("class", "active");
      personnel.setAttribute("class", "show");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane active show");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    },
    aircraft: function aircraft() {
      var air = document.getElementById("air");
      air.setAttribute("class", "nav-link");
      air.setAttribute("class", "active");
      air.setAttribute("class", "show");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane active show");
    },
    events: function events() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      event.setAttribute("class", "active");
      event.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane active show");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    },
    location: function location() {
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      location.setAttribute("class", "active");
      location.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane active show");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
    },
    addaircraft: function addaircraft() {
      var dict = {};
      var sn = this.get('asset').aircraft.length;
      dict['sn'] = sn + 1;
      dict.name = "";
      this.get('asset').aircraft.pushObject(dict);
      this.get('aircraftCompute');
    }
  }
});

exports["default"] = _default;