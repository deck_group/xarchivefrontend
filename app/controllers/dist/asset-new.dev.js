"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  storage: (0, _service.inject)("storage"),
  init: function init() {
    this._super();

    this.get('bus').on('imagedatanew', this, this.recivedImage);
    this.get('bus').on('recognize', this, this.recivedDataForPersonnel);
    this.get('bus').on('asset', this, this.senddata);
  },
  newmodel: {},
  aircraft_sn_id: 1,
  event: [],
  locations: [],
  image: undefined,
  personnals: [],
  aircrafts: [],
  galurl: "",
  ifimageEdited: false,
  urls: (0, _service.inject)("service"),
  imageCompute: (0, _object.computed)('image', function () {
    return this.get('image');
  }),
  personnelCompute: (0, _object.computed)('personnals', function () {
    return this.get('personnals');
  }),
  ifimageEditedComputed: (0, _object.computed)('ifimageEdited', function () {
    return this.get('ifimageEdited');
  }),
  aircraftCompute: (0, _object.computed)('aircrafts', function () {
    return this.get('aircrafts');
  }),
  senddata: function senddata() {
    this.get('bus').sendDataPublish(this.get("personnals"));
  },
  recivedImage: function recivedImage(data) {
    if (data == true) {
      this.set("image", this.get("storage").imagestorage);
      this.get("imageCompute");
      this.set("ifimageEdited", data);
      this.get("ifimageEditedComputed");
    }
  },
  recivedDataForPersonnel: function recivedDataForPersonnel(data) {
    this.set('personnals', undefined);
    this.set('personnals', data);
    this.get('personnelCompute');
  },
  actions: {
    save: function save(newmodel) {
      var _this = this;

      var array = [];
      var newasset = this.get("store").createRecord("asset");

      if (this.get('personnals').length === 0) {
        var dict = {};
        dict['sn'] = "";
        dict['bdno'] = "";
        dict['rank'] = "";
        dict['name'] = "";
        dict['rankinpic'] = "";
        dict['x'] = 0.0;
        dict['y'] = 0.0;
        dict['width'] = 0.0;
        dict['height'] = 0.0;
        dict['isManush'] = false;
        this.get('personnals').push(dict);
      }

      newasset.set('personnel', this.get('personnals'));
      newasset.set('status', "draft");
      var name = document.getElementById("assetid").value + '.jpg';
      newasset.set('assetid', document.getElementById("assetid").value);
      newasset.set('imagename', name);
      newasset.set('imagedataarray', this.get('image'));
      var imageurl = this.get("urls.imageurl") + "/static/images/asset/" + document.getElementById("assetid").value + "/" + name;
      newasset.set('imageurls', imageurl);
      newasset.set('title', newmodel.title);
      newasset.set('taken', newmodel.taken);
      newasset.set('aircraft', this.get('aircrafts'));

      if (this.get("event").length == 0) {
        var dictevent = {};
        dictevent['name'] = "";
        this.get("event").pushObject(dictevent);
      }

      if (this.get("locations").length == 0) {
        var dictevent = {};
        dictevent['name'] = "";
        this.get("locations").pushObject(dictevent);
      }

      newasset.set('event', this.get("event"));
      newasset.set('location', this.get("locations"));

      if (newasset.get('title') !== undefined && newasset.get('taken') !== undefined && this.get('image') !== undefined) {
        newasset.save().then(function () {
          _this.set("image", undefined);

          _this.set('personnals', []);

          _this.set('locations', []);

          _this.set('event', []);

          _this.set("newmodel", {});

          _this.transitionToRoute("asset");
        })["catch"](function (error) {
          _this.get('flashMessages').danger(error, {
            timeout: 3000
          });
        })["finally"]();
      } else {
        if (this.get('image') === undefined) {
          this.get('flashMessages').danger('Asset should have an image', {
            timeout: 3000
          });
        }

        this.get('flashMessages').danger('Check the fields', {
          timeout: 3000
        });
      }
    },
    discardroute: function discardroute() {
      this.set("image", undefined);
      this.set('personnals', []);
      this.set('locations', []);
      this.set('event', []);
      this.set("newmodel", {});
      this.transitionToRoute("asset");
    },
    gallery: function gallery() {
      var gal = document.getElementById("gal");
      gal.setAttribute("class", "nav-link");
      gal.setAttribute("class", "active");
      gal.setAttribute("class", "show");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
      var tabgal = document.getElementById("notebook_page_93");
      tabgal.setAttribute("class", "tab-pane active show");
    },
    events: function events() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      event.setAttribute("class", "active");
      event.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var gal = document.getElementById("gal");
      gal.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane active show");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
      var tabgal = document.getElementById("notebook_page_93");
      tabgal.setAttribute("class", "tab-pane");
    },
    location: function location() {
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      location.setAttribute("class", "active");
      location.setAttribute("class", "show");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var gal = document.getElementById("gal");
      gal.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane active show");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
      var tabgal = document.getElementById("notebook_page_93");
      tabgal.setAttribute("class", "tab-pane");
    },
    personnel: function personnel() {
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      personnel.setAttribute("class", "active");
      personnel.setAttribute("class", "show");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var aircraft = document.getElementById("air");
      aircraft.setAttribute("class", "nav-link");
      var gal = document.getElementById("gal");
      gal.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane active show");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane");
      var tabgal = document.getElementById("notebook_page_93");
      tabgal.setAttribute("class", "tab-pane");
    },
    aircraft: function aircraft() {
      var air = document.getElementById("air");
      air.setAttribute("class", "nav-link");
      air.setAttribute("class", "active");
      air.setAttribute("class", "show");
      var event = document.getElementById("event");
      event.setAttribute("class", "nav-link");
      var location = document.getElementById("location");
      location.setAttribute("class", "nav-link");
      var personnel = document.getElementById("personnel");
      personnel.setAttribute("class", "nav-link");
      var gal = document.getElementById("gal");
      gal.setAttribute("class", "nav-link");
      var tabpersonal = document.getElementById("notebook_page_89");
      tabpersonal.setAttribute("class", "tab-pane");
      var tabevent = document.getElementById("notebook_page_90");
      tabevent.setAttribute("class", "tab-pane");
      var tabloaction = document.getElementById("notebook_page_91");
      tabloaction.setAttribute("class", "tab-pane");
      var tabair = document.getElementById("notebook_page_92");
      tabair.setAttribute("class", "tab-pane active show");
      var tabgal = document.getElementById("notebook_page_93");
      tabgal.setAttribute("class", "tab-pane");
    },
    addaircraft: function addaircraft() {
      var dict = {};
      dict['sn'] = this.get('aircraft_sn_id');
      dict.name = "";
      this.set('aircraft_sn_id', this.get('aircraft_sn_id') + 1);
      this.get('aircrafts').pushObject(dict);
    }
  }
});

exports["default"] = _default;