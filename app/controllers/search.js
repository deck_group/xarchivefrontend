import Controller from '@ember/controller';

export default Controller.extend({
    data: undefined,
    actions: {
        discardroute() {
            this.transitionToRoute("asset")
        },
        advance(){
            this.transitionToRoute("advance")
        },
        search(data) {
            var newdata=data
            this.set("data",undefined)
            localStorage.setItem("search",newdata)
            this.transitionToRoute("searchlist")
        },

    }
});
