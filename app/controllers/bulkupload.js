import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    bus: service("pubsub"),
    newimage: undefined,
    dir: undefined,
    array: [],
    urls: service("service"),
    dircompute: computed(`dir`, function () {
        return this.get(`dir`)
    }),
    newimagecompute: computed(`newimage`, function () {
        return this.get('newimage')
    }),
    init() {
        this._super();
        this.get('bus').on('directoryvaluepub', this, this.directoryvalue);
    },
    directoryvalue(content) {
        this.set('dir', content)
        this.get('dircompute')

    },

    actions: {
        discardroute() {
            this.transitionToRoute('personnels')
        },
        bulkasset() {
            this.transitionToRoute('asset')
        },
        upload() {
            console.log("hello")
            var self = this;
            const reader = new FileReader();
            const file = event.target.files[0];
            let imageData;
            reader.onload = function () {
                imageData = reader.result;
                var image = new Image();
                image.src = imageData;
                image.onload = function () {

                    if (image.width > 1000) {
                        var dict = {}
                        var heightr = parseFloat(image.height / image.width)
                        var newwidth = 1000
                        var newheight = 1000 * heightr
                        var canvas = document.createElement("canvas");
                        var ctx = canvas.getContext("2d");

                        canvas.width = newwidth; // target width
                        canvas.height = newheight; // target height
                        ctx.drawImage(image,
                            0, 0, image.width, image.height,
                            0, 0, canvas.width, canvas.height
                        );
                        var resampledImage = new Image();
                        resampledImage.src = canvas.toDataURL();
                        dict['asset'] = resampledImage.src
                        dict['originalasset'] = imageData
                        dict['name'] = file.name
                        self.set('newimage', undefined)
                        self.get('array').push(dict)
                        self.set('newimage', self.get('array'))
                        self.get('newimagecompute')

                    }
                    else {
                        var dict = {}
                        dict['asset'] = imageData
                        dict['originalasset'] = imageData
                        dict['name'] = file.name
                        self.set('newimage', undefined)
                        self.get('array').push(dict)
                        self.set('newimage', self.get('array'))
                        self.get('newimagecompute')
                    }

                };


            };
            if (file) {
                reader.readAsDataURL(file);
            }
        },
        saveasset(newimage) {
            var newarray = []
            if (newimage === undefined) {
                this.get('flashMessages').danger('No image uploaded', { timeout: 3000 })
            }
            else {
                document.getElementById("overlay").style.display = "block";
                for (var i = 0; i < newimage.length; i++) {
                    var alldict = {}
                    var aircraft = [
                        { "sn": 0, "name": "" }
                    ]
                    var personel = []
                    var dict = {}
                    dict['sn'] = ``
                    dict['bdno'] = ``
                    dict['rank'] = ``
                    dict['name'] = ``
                    dict['rankinpic'] = ``
                    dict['x'] = 0.0
                    dict['y'] = 0.0
                    dict['width'] = 0.0
                    dict['height'] = 0.0
                    dict['isManush'] = false
                    personel.push(dict)
                    var assetid = Math.random().toString(36).slice(2)
                    alldict['assetid'] = assetid
                    var spltext = newimage[i]['name'].split(".")
                    var oriname = spltext[0] + "_" + assetid
                    alldict['imagename'] = spltext[0]
                    alldict['originalimagename'] = oriname
                    alldict['isSearch'] = "0"
                    alldict['isrecognised'] = "0"
                    alldict['status'] = "draft"

                    var imageurl = this.get("urls.imageurl") + `/static/asset/` + spltext[0] + ".jpg"
                    alldict['imageurls'] = imageurl
                    alldict['title'] = `N/A`
                    alldict['taken'] = `N/A`
                    alldict['description'] = `N/A`
                    alldict['aircraft'] = aircraft
                    alldict['personnel'] = personel

                    var dictevent = {}
                    dictevent['name'] = ``
                    var event = []
                    event.push(dictevent)
                    alldict['event'] = event

                    var location = []
                    var dictlocation = {}
                    dictlocation['name'] = ``
                    location.push(dictlocation)
                    alldict['location'] = location
                    alldict['imagedataarray'] = newimage[i]['asset']
                    alldict['secrecylevel'] = `N/A`
                    alldict['studiophotography'] = `N/A`

                    var dicttags = {}
                    dicttags['name'] = ``
                    var othertags = []
                    othertags.pushObject(dicttags)

                    alldict['othertags'] = othertags
                    var dictcom = {}
                    dictcom['sn'] = 0
                    dictcom["commentFrom"] = ``
                    dictcom["dt"] = ``
                    dictcom["comment"] = ``
                    var comments = []
                    comments.push(dictcom)

                    alldict['comment'] = comments
                    alldict['caption'] = "N/A"
                    alldict['directory'] = this.get("dir")
                    alldict['galurl'] = `N/A`

                    var originalimageurl = this.get("urls.imageurl") + `/static/assetoriginal/` + oriname + ".jpg"

                    alldict['originalimageurl'] = originalimageurl
                    alldict['originalimagename'] = oriname
                    alldict['oriimagedataarray'] = newimage[i]["originalasset"]
                    newarray.push(alldict)
                }
                var that = this
                if (this.get("dir") !== undefined) {
                    var hurl = this.get("urls.url") + '/bulkassets'
                    $.ajax({
                        method: "POST",
                        url: hurl,
                        data: {
                            data: JSON.stringify(newarray)

                        }
                    }).then(bind(this, (data) => {
                        document.getElementById("overlay").style.display = "none";
                        that.set("newimage", undefined)
                        that.set('array', [])
                        window.location.reload(true);
                    }))

                }
                else {
                    document.getElementById("overlay").style.display = "none";
                    this.get('flashMessages').danger('Should select a Directory', { timeout: 3000 })
                }
            }


        }
    }
});
