import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';


export default Controller.extend({
    newmodel: {
        'bdno': ``,
        'name': ``,
        'currentrank': ``,
        'isretired': false
    },
    urls: service("service"),
    sn: 1,
    imagearray: [],
    rankhistory: {},
    imageurl: [],
    componentNames: [],
    componentNamesCom: computed('componentNames', function () {
        return this.get('componentNames')
    }),
    ranks: computed(function () {
        return this.get("store").findAll("rank")
    }),
    actions: {
        personnelroute() {
            this.set("newmodel", {})
            this.set("imgGallery", [])
            this.set("componentNames", [])
            this.transitionToRoute("personnels")
        },


        save(status) {
            document.getElementById("overlay").style.display = "block";
            var imagearray = []
            let newpersonnel = this.get("store").createRecord("personnel")

            if (this.get('componentNames').length === 0) {
                var array = []
                var dict = {}
                dict['sn'] = 0
                dict.rank = ``
                dict.startdate = ``
                dict.enddate = ``
                array.pushObject(dict);
                newpersonnel.set("rankhistory", array)
            }
            else {
                newpersonnel.set("rankhistory", this.get('componentNames'))
            }
            newpersonnel.set("isretired", this.get('newmodel').isretired)
            if (this.get('imagearray').length > 0
                && this.get('newmodel').bdno !== ``
                && this.get('newmodel').name !== ``
                && this.get('newmodel').currentrank !== ``
            ) {
                newpersonnel.set("bdno", this.get('newmodel').bdno)
                newpersonnel.set("name", this.get('newmodel').name)
                newpersonnel.set("currentrank", this.get('newmodel').currentrank)
                newpersonnel.set('imagedataarray', this.get('imagearray'))
                for (var j = 0; j < newpersonnel.get('imagedataarray').length; j++) {
                    var imgedict = {}
                    var imagename = 'personnel-' + this.get('newmodel').bdno + "-" + newpersonnel.get('imagedataarray')[j]['imagename']
                    newpersonnel.get('imagedataarray')[j]['imagename'] = imagename
                    var imageurl = this.get("urls.imageurl") + '/static'
                    imgedict['url'] = imageurl + '/' + imagename
                    this.get('imageurl').push(imgedict)
                    var newdict = {}
                    newdict['bdno'] = this.get('newmodel').bdno
                    newdict['imagename'] = imagename
                    newdict['imagedirectory'] = imageurl + '/' + imagename
                    imagearray.push(newdict)
                }
                newpersonnel.set('imageurls', this.get('imageurl'))
                newpersonnel.set("status", status)
                newpersonnel.set("nameplate", this.get('newmodel').nameplate)
                for (var x = 0; x < imagearray.length; x++) {
                    let newimage = this.get("store").createRecord("imagereference")
                    newimage.set('bdno', imagearray[x]['bdno'])
                    newimage.set('imagename', imagearray[x]['imagename'])
                    newimage.set('imagedirectory', imagearray[x]['imagedirectory'])
                    newimage.save().then(() => { }).catch((error) => { this.get('flashMessages').danger(error, { timeout: 3000 }) }).finally({})

                }
                newpersonnel.save().then(() => {
                    document.getElementById("overlay").style.display = "none";
                    this.set("newmodel", {})
                    this.set("imgGallery", [])
                    this.set("componentNames", [])
                    this.transitionToRoute('personnels')

                })
                    .catch((error) => {
                        document.getElementById("overlay").style.display = "none";
                        this.transitionToRoute('personnels')
                        // this.get('flashMessages').danger(error, { timeout: 3000 }) 
                    }

                    ).finally({})

            } else {
                if (this.get('imagearray').length === 0) {
                    this.get('flashMessages').danger('Person should have atleast one image', { timeout: 3000 })
                }
                this.get('flashMessages').danger('Check the fields', { timeout: 3000 })

            }

        },
        discardroute() {
            this.set("newmodel", {})
            this.set("imgGallery", [])
            this.set("componentNames", [])
            this.transitionToRoute("personnels")
        },

        addEventColumn() {
            var dict = {}
            dict['sn'] = this.get('sn')
            dict.rank = ``
            dict.startdate = ``
            dict.enddate = ``
            this.set('sn', this.get('sn') + 1)
            this.get('componentNames').pushObject(dict);
        },


    },



});
