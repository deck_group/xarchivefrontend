import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
    bus: service("pubsub"),
    storage: service("storage"),
    isAspectRatio: true,
    width: 0,
    height: 0,
    isGrey: false,
    imageComponent: computed('image', function () {
        return this.get('image')
    }),
    heightComponent: computed('height', function () {
        return this.get('height')
    }),
    widthComponent: computed('width', function () {
        return this.get('width')
    }),

    oriheightComponent: computed('originalheight', function () {
        return this.get('originalheight')
    }),
    oriwidthComponent: computed('originalwidth', function () {
        return this.get('originalwidth')
    }),

    invertedComponent: computed('isInverted', function () {
        return this.get('isInverted')
    }),

    aspectComponent: computed('width', function () {
        return this.get('isAspectRatio')
    }),
    truncateColor(value) {
        if (value < 0) {
            value = 0;
        } else if (value > 255) {
            value = 255;
        }

        return value;
    },
    init() {
        this._super();
        this.get('bus').on('imageparameter', this, this.recivedImageParameter)
        this.get('bus').on('aspectratio', this, this.recivedAspectRatio)
        this.get('bus').on('imageCorrectionOption', this, this.recivedImageCorrectionOption)
    },
    recivedImageCorrectionOption(data, fl) {
        if (fl === "invert") {
            this.send("invertImage", data)
        } else if (fl === "brightness") {
            this.send("fnbrightness", data)

        }
        else if (fl === "contrast") {
            this.send("fncontrast", data)
        }
        else if (fl === "grey") {
            this.send("greyImage", data)
        }
        else if (fl === "hue") {
            this.send("fnhue", data)
        }
        else if (fl === "saturation") {
            this.send("fnsaturation", data)
        }

        else if (fl === "lightness") {
            this.send("fnlightness", data)
        }
    },
    recivedAspectRatio(data) {
        this.set("isAspectRatio", data)
        this.get("aspectComponent")
    },

    recivedImageParameter(data, fl) {
        this.set("originalheight", this.get("storage").height)
        this.set("originalwidth", this.get("storage").width)
        this.get("oriheightComponent")
        this.get("oriwidthComponent")
        if (fl === `height`) {
            var newh = ((this.get("storage").height / this.get("storage").width) * data)
            this.set("height", parseInt(newh))
            this.get("heightComponent")
        } else {
            var newWidth = ((this.get("storage").width / this.get("storage").height) * data)
            this.set("width", parseInt(newWidth))
            this.get("widthComponent")
        }

    },


    actions: {

        greyImage(fl) {
            var img = this.get("image")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')

            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            var imageData = context.getImageData(0, 0, canvas.width, canvas.height)
            for (var i = 0; i < imageData.data.length; i += 4) {
                var avg = (imageData.data[i] + imageData.data[i + 1] + imageData.data[i + 2]) / 3;
                imageData.data[i] = avg;
                imageData.data[i + 1] = avg;
                imageData.data[i + 2] = avg;
            }
            context.putImageData(imageData, 0, 0);
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.set('image', resizedImage)
            this.set("oriimage", resizedImage)

        },
        fnlightness(data) {
            var img = this.get("oriimage")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')
            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            context.globalCompositeOperation = data < 100 ? "color-burn" : "color-dodge";
            var colr = 'hsl(' + this.get("storage").hue + ',' + this.get("storage").saturation + '%,' + data + '%)'
            context.fillStyle = colr
            context.fillRect(0, 0, image.width, image.height);
            context.globalCompositeOperation = "source-over";
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.get("storage").set("lighness", data)
            this.set('image', resizedImage)


        },
        fnhue(data) {
            var img = this.get("oriimage")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')
            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            context.globalCompositeOperation = 'hue'

            var colr = 'hsl(' + data + ',' + this.get("storage").saturation + '%,' + this.get("storage").lighness + '%)'
            console.log(colr)
            context.fillStyle = colr
            context.fillRect(0, 0, image.width, image.height);
            context.globalCompositeOperation = "source-over";
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.get("storage").set("hue", data)
            this.set('image', resizedImage)


        },

        fnsaturation(data) {
            var img = this.get("oriimage")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')
            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            context.globalCompositeOperation = 'saturation'
            var colr = 'hsl(' + this.get("storage").hue + ',' + data + '%,' + this.get("storage").lighness + '%)'
            context.fillStyle = colr
            context.fillRect(0, 0, image.width, image.height);
            context.globalCompositeOperation = "source-over";
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.get("storage").set("saturation", data)
            this.set('image', resizedImage)

        },

        fncontrast(data) {
            var contrast = parseInt(data, 10);
            var factor = (259.0 * (contrast + 255.0)) / (255.0 * (259.0 - contrast));
            var img = this.get("oriimage")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')
            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            var imageData = context.getImageData(0, 0, canvas.width, canvas.height)
            for (var i = 0; i < imageData.data.length; i += 4) {
                imageData.data[i] = this.truncateColor(factor * (imageData.data[i] - 128.0) + 128.0);
                imageData.data[i + 1] = this.truncateColor(factor * (imageData.data[i + 1] - 128.0) + 128.0);
                imageData.data[i + 2] = this.truncateColor(factor * (imageData.data[i + 2] - 128.0) + 128.0);
            }
            context.putImageData(imageData, 0, 0);
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.set('image', resizedImage)
            this.get("storage").set("contrast", contrast)

        },

        fnbrightness(data) {
            var brightness = parseInt(data, 10);
            var img = this.get("oriimage")
            var image = new Image();
            image.crossOrigin = 'Anonymous'
            image.setAttribute('crossOrigin', 'anonymous');
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')
            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            var imageData = context.getImageData(0, 0, canvas.width, canvas.height)
            for (var i = 0; i < imageData.data.length; i += 4) {
                imageData.data[i] += 255 * (brightness / 100);
                imageData.data[i + 1] += 255 * (brightness / 100);
                imageData.data[i + 2] += 255 * (brightness / 100);
            }
            context.putImageData(imageData, 0, 0);
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.set('image', resizedImage)
            this.get("storage").set("brigthness", brightness)



        },

        invertImage(fl) {
            var img = this.get("image")
            var image = new Image();
            image.src = img
            var canvas = document.createElement('canvas')
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext('2d')

            context.drawImage(image, 0, 0, canvas.width, canvas.height)
            var imageData = context.getImageData(0, 0, canvas.width, canvas.height)
            for (var i = 0; i < imageData.data.length; i += 4) {
                imageData.data[i] = imageData.data[i] ^ 255;
                imageData.data[i + 1] = imageData.data[i + 1] ^ 255;
                imageData.data[i + 2] = imageData.data[i + 2] ^ 255;
            }
            context.putImageData(imageData, 0, 0);
            var resizedImage = canvas.toDataURL('image/jpeg');
            this.set('image', resizedImage)
            this.set("oriimage", resizedImage)

        },
        resize() {
            var img = this.get("image")
            var image = new Image();
            image.src = img
            var resizedImage;
            var canvas = document.createElement('canvas')
            canvas.width = this.get("width");
            canvas.height = this.get("height");
            canvas.getContext('2d').drawImage(image, 0, 0, this.get("width"), this.get("height"));
            resizedImage = canvas.toDataURL('image/jpeg');
            this.set('image', resizedImage)
            this.set("oriimage", resizedImage)

        },
        discardroute() {
            this.get('storage').set("imagestorage", undefined)
            if (this.get('storage').parameter === undefined) {
                this.transitionToRoute(this.get('storage').route)
            } else {
                this.transitionToRoute(this.get('storage').route, this.get('storage').parameter)
            }
        },
        save() {
            this.get('storage').set("imagestorage", this.get("image"))
            this.set("width", 0)
            this.set("height", 0)
            this.get('bus').sendImageDataPublish(true);
            if (this.get('storage').parameter === undefined) {
                this.transitionToRoute(this.get('storage').route)
            }
            else {
                this.transitionToRoute(this.get('storage').route, this.get('storage').parameter)
            }


        }
    }


});
