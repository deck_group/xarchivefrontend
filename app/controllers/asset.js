import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    bus: service("pubsub"),
    selectedDir: undefined,
    init() {
        this._super();
        this.get('bus').on('selectedDirectory', this, this.recivedData);
    },
    recivedData(data) {
        this.set(`selectedDir`, data)
        this.transitionToRoute(`assetparam`,data)
    },



    actions: {
        createroute() {
            this.transitionToRoute("asset-new")
        },

        discardroute() {
            this.transitionToRoute("personnels")
        }

    }
});
