import Controller from '@ember/controller';

export default Controller.extend({
    bdno: [],
    titles:[],
    names:[],
    event:[],
    locations:[],
    operation: ``,
    startdt:undefined,
    enddt:undefined,

    actions: {
        discardroute() { },
        choosentitle(oper) {
            this.set("operation", oper)
        },
        searchtitle() {
            var bdnotxt=``
            var titlestxt=``
            var namestxt=``
            var locationstxt=``
            var eventtxt=``
            if(this.get("operation")===``){

                localStorage.setItem("operation","or")
            }else{
                localStorage.setItem("operation",this.get("operation"))
            }
            for(var b=0 ; b<this.get("bdno").length; b++){
                bdnotxt=bdnotxt+this.get("bdno")[b][`name`]+`,`
            }
            bdnotxt = bdnotxt.slice(0, -1);

            for(var t=0 ; t<this.get("titles").length; t++){
                titlestxt=titlestxt+this.get("titles")[t][`name`]+`,`
            }
            titlestxt = titlestxt.slice(0, -1);
           

            for(var n=0 ; n<this.get("names").length; n++){
                namestxt=namestxt+this.get("names")[n][`name`]+`,`
            }
            namestxt = namestxt.slice(0, -1);

            for(var l=0 ; l<this.get("locations").length; l++){
                locationstxt=locationstxt+this.get("locations")[l][`name`]+`,`
            }
            locationstxt = locationstxt.slice(0, -1);
            
            for(var e=0 ; e<this.get("event").length; e++){
                eventtxt=eventtxt+this.get("event")[e][`name`]+`,`
            }
            eventtxt = eventtxt.slice(0, -1);
            localStorage.setItem("advance","true")
            var url=`/advancesearches?operator=`+localStorage.getItem("operation")

            if(bdnotxt !== ``){
                url=url+`&bdno=`+bdnotxt
            }
            if(titlestxt !== ``){
                url=url+`&titles=`+titlestxt
            }
            if(namestxt !== ``){
                url=url+`&names=`+namestxt
            }
            if(locationstxt !== ``){
                url=url+`&location=`+locationstxt
            }

            if(eventtxt !== ``){
                url=url+`&events=`+eventtxt
            }

            if(this.get("startdt") !== undefined){
                if(this.get("enddt") !==undefined){
                    url=url+`&startdate=`+ this.get("startdt") +`&enddate=` + this.get("enddt")
                }
            }
            localStorage.setItem("url",url)
            this.set("bdno",[])
            this.set("titles",[])
            this.set("names",[])
            this.set("event",[])
            this.set("locations",[])
            this.set("startdt",undefined)
            this.set("enddt",undefined)
            this.transitionToRoute("advancesearchresult")
        }
    }
});
