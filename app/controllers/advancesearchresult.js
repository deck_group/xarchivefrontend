import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    actions:{
        discardroute(){
            this.transitionToRoute("advancesearchresult")
        },
        detail(id){
            this.transitionToRoute('asset-view', id)
        }
    }
});
