import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 5,
    bus: service("pubsub"),
    selectedDir: undefined,
    init() {
        this._super();
        this.get('bus').on('selectedDirectory', this, this.recivedData);
    },
    recivedData(data) {
        this.set(`selectedDir`, data)
        this.transitionToRoute(`assetparam`,data)
    },

    actions: {
        createroute() {
            this.transitionToRoute("asset-new")
        },
        detail(id) {
            this.transitionToRoute('asset-view', id)
        },
        discardroute() {
            this.transitionToRoute("personnels")
        }

    }


});
