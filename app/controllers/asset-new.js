import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';


export default Controller.extend({
    bus: service("pubsub"),
    storage: service("storage"),
    init() {
        this._super();
        this.get('bus').on('imagedatanew', this, this.recivedImage);
        this.get('bus').on('recognize', this, this.recivedDataForPersonnel);
        this.get('bus').on('asset', this, this.senddata);
    },
    componentNames: [],
    sn: 1,
    newmodel: {},
    aircraft_sn_id: 1,
    event: [],
    othertags: [],
    locations: [],
    image: undefined,
    personnals: [],
    aircrafts: [],
    galurl: `/BAFArchive/`,
    ifimageEdited: false,
    urls: service("service"),
    imageCompute: computed('image', function () {
        return this.get('image')
    }),
    personnelCompute: computed('personnals', function () {
        return this.get('personnals')
    }),

    ifimageEditedComputed: computed('ifimageEdited', function () {
        return this.get('ifimageEdited')
    }),


    aircraftCompute: computed('aircrafts', function () {
        return this.get('aircrafts')
    }),
    senddata() {
        this.get('bus').sendDataPublish(this.get("personnals"));
    },

    recivedImage(data) {
        if (data == true) {
            this.set("image", this.get("storage").imagestorage)
            this.get("imageCompute")
            this.set("ifimageEdited", data)
            this.get("ifimageEditedComputed")
        }

    },
    recivedDataForPersonnel(data) {
        this.set('personnals', undefined)
        this.set('personnals', data)
        this.get('personnelCompute')
    },
    actions: {
        addEventColumn() {
            var dict = {}
            dict['sn'] = this.get('sn')
            dict.commentFrom = localStorage.getItem("user")
            dict.dt = moment(new Date()).format("YYYY-MM-DD")
            dict.comment = ``
            this.set('sn', this.get('sn') + 1)
            this.get('componentNames').pushObject(dict);
        },
        save(newmodel,assetid) {
            document.getElementById("overlay").style.display = "block";
            var newasset = this.get("store").createRecord("asset");
            newasset.set("galurl", this.get("galurl"))
            newasset.set("secrecylevel", newmodel.secrecylevel)
            if (newmodel.description !== undefined) {
                newasset.set("description", newmodel.description)
            }
            else {
                newasset.set("description", `N/A`)
            }
            if (newmodel.caption === undefined) {
                newasset.set("caption", `N/A`)
            }
            else {
                newasset.set("caption", newmodel.caption)
            }

            if (newmodel.studiophotography === undefined) {
                newasset.set("studiophotography", `N/A`)
            }
            else {
                newasset.set("studiophotography", newmodel.studiophotography)
            }

            if (this.get("componentNames").length === 0) {
                var arraycomments = []
                var dict = {}
                dict.sn = 0
                dict.commentFrom = ``
                dict.dt = ``
                dict.comment = ``
                arraycomments.push(dict)
                newasset.set("comment", arraycomments)

            }
            else {
                newasset.set("comment", this.get("componentNames"))
            }


            if (this.get('personnals').length === 0) {
                var dict = {}
                dict['sn'] = ``
                dict['bdno'] = ``
                dict['rank'] = ``
                dict['name'] = ``
                dict['rankinpic'] = ``
                dict['x'] = 0.0
                dict['y'] = 0.0
                dict['width'] = 0.0
                dict['height'] = 0.0
                dict['isManush'] = false
                this.get('personnals').push(dict)


            }

            newasset.set('personnel', this.get('personnals'))
            newasset.set('status', `draft`)
            var name = 'asset_' + assetid + '.jpg'
            newasset.set('assetid', assetid)
            newasset.set('imagename', name)
            newasset.set('imagedataarray', this.get('image'))
            var imageurl = this.get("urls.imageurl") + `/static/`+ name
            newasset.set('imageurls', imageurl)
            newasset.set('title', newmodel.title)
            newasset.set('taken', newmodel.taken)
            newasset.set('aircraft', this.get('aircrafts'))
            if (this.get("event").length == 0) {
                var dictevent = {}
                dictevent['name'] = ``
                this.get("event").pushObject(dictevent)
            }

            if (this.get("locations").length == 0) {
                var dictevent = {}
                dictevent['name'] = ``
                this.get("locations").pushObject(dictevent)
            }

            if (this.get("othertags").length == 0) {
                var dicttags = {}
                dicttags['name'] = ``
                this.get("othertags").pushObject(dicttags)
            }

            newasset.set('event', this.get("event"))
            newasset.set('location', this.get("locations"))
            newasset.set('othertags', this.get("othertags"))
            newasset.set('directory', newmodel.directory)
            if (newasset.get('title') !== undefined && newasset.get('taken') !== undefined && this.get('image') !== undefined && newasset.get("secrecylevel") !== undefined) {
                newasset.save().then(() => {
                    document.getElementById("overlay").style.display = "none";
                    this.set("image", undefined)
                    this.set('personnals', [])
                    this.set('locations', [])
                    this.set('event', [])
                    this.set("newmodel", {})
                    this.set('othertags',[])
                    this.set("componentNames", [])
                    this.transitionToRoute("asset")
                })
                .catch((error) => {

                    this.get('flashMessages').danger(error, { timeout: 3000 })
                }).finally()
            }
            else {
                if (this.get('image') === undefined) {
                    document.getElementById("overlay").style.display = "none";
                    this.get('flashMessages').danger('Asset should have an image', { timeout: 3000 })
                }
                this.get('flashMessages').danger('Check the fields', { timeout: 3000 })
            }

        },

        discardroute() {
            this.set("image", undefined)
            this.set('personnals', [])
            this.set('locations', [])
            this.set('event', [])
            this.set("othertags", [])
            this.set("componentNames", [])
            this.set("newmodel", {})
            this.transitionToRoute("asset")
        },

        commentTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            comments.setAttribute("class", "active")
            comments.setAttribute("class", "show")

            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane active show")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")


            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        otherTagsTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")
            other.setAttribute("class", "active")
            other.setAttribute("class", "show")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane active show")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        studioTab() {
            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")
            studio.setAttribute("class", "active")
            studio.setAttribute("class", "show")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane active show")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        gallery() {

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")
            gal.setAttribute("class", "active")
            gal.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")


            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane active show")
        },


        events() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            event.setAttribute("class", "active")
            event.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane active show")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },

        location() {
            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")
            location.setAttribute("class", "active")
            location.setAttribute("class", "show")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane active show")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")

        },
        personnel() {
            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")
            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")
            personnel.setAttribute("class", "active")
            personnel.setAttribute("class", "show")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var aircraft = document.getElementById("air")
            aircraft.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")


            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane active show")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")
        },
        aircraft() {
            var air = document.getElementById("air")
            air.setAttribute("class", "nav-link")
            air.setAttribute("class", "active")
            air.setAttribute("class", "show")

            var event = document.getElementById("event")
            event.setAttribute("class", "nav-link")

            var location = document.getElementById("location")
            location.setAttribute("class", "nav-link")

            var personnel = document.getElementById("personnel")
            personnel.setAttribute("class", "nav-link")

            var gal = document.getElementById("gal")
            gal.setAttribute("class", "nav-link")

            var studio = document.getElementById("studio")
            studio.setAttribute("class", "nav-link")

            var other = document.getElementById("othertags")
            other.setAttribute("class", "nav-link")

            var comments = document.getElementById("comment")
            comments.setAttribute("class", "nav-link")
            var tabscomment = document.getElementById("notebook_page_96")
            tabscomment.setAttribute("class", "tab-pane")

            var tabsother = document.getElementById("notebook_page_95")
            tabsother.setAttribute("class", "tab-pane")

            var tabstd = document.getElementById("notebook_page_94")
            tabstd.setAttribute("class", "tab-pane")

            var tabpersonal = document.getElementById("notebook_page_89")
            tabpersonal.setAttribute("class", "tab-pane")

            var tabevent = document.getElementById("notebook_page_90")
            tabevent.setAttribute("class", "tab-pane")

            var tabloaction = document.getElementById("notebook_page_91")
            tabloaction.setAttribute("class", "tab-pane")

            var tabair = document.getElementById("notebook_page_92")
            tabair.setAttribute("class", "tab-pane active show")

            var tabgal = document.getElementById("notebook_page_93")
            tabgal.setAttribute("class", "tab-pane")



        },

        addaircraft() {
            var dict = {}
            dict['sn'] = this.get('aircraft_sn_id')
            dict.name = ``
            this.set('aircraft_sn_id', this.get('aircraft_sn_id') + 1)
            this.get('aircrafts').pushObject(dict);

        },

    }
});
