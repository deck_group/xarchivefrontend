import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    queryParams: ['pageNumber'],
    bus: service("pubsub"),
    urls: service("service"),
    pageNumber: 0,
    pageSize: 10,
    store: service(),
    more: false,
    currentPages: 1,
    istheremore: undefined,
    isPrev: false,
    allassets: [],
    islast: false,
    morelength: 0,
    moreassets: undefined,
    selectedDir: undefined,
    store: service(),

    init() {
        this._super();
        this.get('bus').on('selectedDirectory', this, this.recivedData);
    },
    recivedData(data) {
        this.set(`selectedDir`, data)
        this.get(`gallariesCompute`)
    },

    gallariesCompute: computed(`selectedDir`, function () {
        var dir = this.get('selectedDir')
        this.set("istheremore", undefined)
        this.set("isPrev", false)
        if (dir === "/BAFArchive/") {
            var hurl = this.get("urls.url") + '/galleries'
            $.ajax({
                method: "GET",
                url: hurl
            }).then(bind(this, (data) => {
                var galleries=data["data"]
                this.set('counts', galleries.count);
                this.set('galleries', galleries.result);
                this.set('next_url', galleries.next_url);
                this.set('previous_url', galleries.previous_url);
                this.get("pages")
            }))
        }
        else {
            var hurl = this.get("urls.url") + '/gallerydir'
            $.ajax({
                method: "POST",
                url: hurl,
                data: {
                    dir: dir

                }
            }).then(bind(this, (data) => {
                this.set("galleries", data["result"])
                this.get("pages")
            }))
        }

    }),

    pages: Ember.computed('galleries', 'pageSize', 'pageSize', 'more', 'moreassets', 'islast', 'morelength', function () {
        var pages = []
        if (this.get("more") === true) {
            this.set("pageNumber", 0)
            var arrangedContent = this.get('moreassets')
            while (arrangedContent.length > 0) {
                pages.push(arrangedContent.splice(0, this.get('pageSize')));
            }
        }
        else {
            var arrangedContent = this.get('galleries')
            this.set('moreassets', [])
            while (arrangedContent.length > 0) {
                pages.push(arrangedContent.splice(0, this.get('pageSize')));
            }
        }
        if (this.get("islast") === true) {
            if (parseInt(this.get("morelength")) > 0) {
                this.set("allassets", [])
                for (var i = 0; i < this.get("morelength"); i++) {
                    this.get("allassets").push(i)
                }
            }
            this.set("istheremore", undefined)
            this.get("istheremoreflagone")

        }
        else {
            var limit = this.get("counts") / this.get("pageSize")
            if (this.get("allassets").length + 1 < Math.ceil(limit)) {
                this.set("istheremore", Math.ceil(limit))
                this.get("istheremoreflagone")
            }
            else {
                this.set("istheremore", undefined)
                this.get("istheremoreflagone")
            }
            this.get("isPrevCompute")

        }

        this.get("allassetsCompute")
        return pages;
    }),
    allassetsCompute: Ember.computed('allassets', function () {
        return this.get("allassets")

    }),
    isPrevCompute: Ember.computed('isPrev', function () {
        return this.get("isPrev")

    }),
    istheremoreflagone: Ember.computed('istheremore', function () {
        return this.get("istheremore")

    }),
    paginatedContent: Ember.computed('pages', 'pageNumber', function () {
        return this.get('pages')[this.get('pageNumber')]
    }),
    actions: {
        detail(id) {
            this.transitionToRoute('asset-view', id)
        },
        discardroute() {
            this.transitionToRoute("asset")
        },
        last(hurl, istheremore) {
            var lastpage = Math.ceil(this.get("counts") / 30)
            this.set("islast", true)
            var url = hurl + "?page=" + lastpage.toString()
            $.ajax({
                method: "GET",
                url: url
            }).then(bind(this, (data) => {
                if (data['data']['result'].length > 0) {
                    this.set("more", false)
                    this.set('pageNumber', 0)
                    this.set('morelength', istheremore)
                    this.set("galleries", data['data']['result'])
                    this.set("next_url", data['data']['next_url'])
                    this.set("previous_url", data['data']['previous_url'])
                    this.set("isPrev", true)
                    this.get("paginatedContent")
                } else {
                    this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                }

            }))

        },

        prevClicked(prev_url) {
            this.set("islast", false)
            if (this.get("pageNumber") > 0) {
                this.set('pageNumber', this.get('pageNumber') - 1)
            }
            else {
                if (prev_url === null) {
                    this.set("isPrev", false)
                    this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                }
                else {
                    this.set("currentPages", this.get("currentPages") - 1)
                    var arr = this.get('allassets');
                    var n = 5;
                    arr.splice(arr.length - n);
                    if (this.get("pages").length > 0) {
                        this.set("pageNumber", this.get("pages").length - 1)
                    }
                    else {
                        this.set("pageNumber", this.get("pages").length)
                    }
                    this.set('allassets', arr)
                    $.ajax({
                        method: "GET",
                        url: prev_url
                    }).then(bind(this, (data) => {
                        if (data['data']['result'].length > 0) {
                            this.set("more", false)
                            this.set("galleries", data['data']['result'])
                            this.set("next_url", data['data']['next_url'])
                            this.set("previous_url", data['data']['previous_url'])
                            if (data['data']['previous_url'] === null) {
                                this.set("isPrev", false)
                            } else {
                                this.set("isPrev", true)
                            }
                            this.get("paginatedContent")
                        } else {
                            this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                        }

                    }))
                }
            }
        },

        moreprev(prev_url) {
            this.set("islast", false)
            if (prev_url === null) {
                this.set("isPrev", false)
                this.get('flashMessages').success("There is no more result", { timeout: 1500 })
            }
            else {
                this.set("currentPages", this.get("currentPages") - 1)
                var arr = this.get('allassets');
                var n = 5;
                arr.splice(arr.length - n);
                if (this.get("pages").length > 0) {
                    this.set("pageNumber", this.get("pages").length - 1)
                } else {
                    this.set("pageNumber", this.get("pages").length)
                }

                this.set('allassets', arr)
                $.ajax({
                    method: "GET",
                    url: prev_url
                }).then(bind(this, (data) => {
                    if (data['data']['result'].length > 0) {
                        this.set("more", false)
                        this.set("galleries", data['data']['result'])
                        this.set("next_url", data['data']['next_url'])
                        this.set("previous_url", data['data']['previous_url'])
                        if (data['data']['previous_url'] === null) {
                            this.set("isPrev", false)
                        } else {
                            this.set("isPrev", true)
                        }
                        this.get("paginatedContent")
                    } else {
                        this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                    }

                }))
            }
        },

        moredata(next_url) {
            this.set("islast", false)
            if (next_url === null) {
                this.get('flashMessages').success("There is no more result", { timeout: 1500 })
            } else {
                var pages = this.get("pages")
                for (var i = 0; i < pages.length; i++) {
                    this.get('allassets').push(pages[i])
                }
                this.set("isPrev", true)
                this.set("currentPages", this.get("currentPages") + 1)
                $.ajax({
                    method: "GET",
                    url: next_url
                }).then(bind(this, (data) => {
                    if (data['data']['result'].length > 0) {
                        this.set("more", true)
                        this.set("moreassets", data['data']['result'])
                        this.set("next_url", data['data']['next_url'])
                        this.set("previous_url", data['data']['previous_url'])
                        this.get("paginatedContent")
                    } else {
                        this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                    }

                }))
            }

        },

        nextClicked(next_url) {
            this.set("islast", false)
            if (this.get("pages").length > this.get("pageNumber") + 1) {
                this.set('pageNumber', this.get('pageNumber') + 1)
            }
            else {
                if (next_url === null) {
                    this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                } else {
                    var pages = this.get("pages")
                    for (var i = 0; i < pages.length; i++) {
                        this.get('allassets').push(pages[i])
                    }
                    this.set("isPrev", true)
                    this.set("currentPages", this.get("currentPages") + 1)
                    $.ajax({
                        method: "GET",
                        url: next_url
                    }).then(bind(this, (data) => {
                        if (data['data']['result'].length > 0) {
                            this.set("more", true)
                            this.set("moreassets", data['data']['result'])
                            this.set("next_url", data['data']['next_url'])
                            this.set("previous_url", data['data']['previous_url'])
                            this.get("paginatedContent")
                        } else {
                            this.get('flashMessages').success("There is no more result", { timeout: 1500 })
                        }

                    }))
                }
            }




        }

    },


});
