import Mixin from '@ember/object/mixin';

export default Mixin.create({

    validate: function (value, isPresence, label, afterdatelabel, beforedatelabel) {
        var msgArray = []
        if (isPresence === true) {
            msgArray.push(this.presence(value, label))
        }
        msgArray.push(this.invalid(value, label, beforedatelabel, afterdatelabel))
        
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },

    presence(value, label) {
        var msg = ''
        if (value === undefined) {
            msg = label + ` is required`
        }
        return msg

    },

    invalid(value, label, before, after) {
        var msg = ''
        if (value !== undefined) {
            var check = new Date(value)
            before.setDate(before.getDate() + 1)
            var afterdt = new Date(after)
            if (check > afterdt && check < before) { }
            else {
                msg = label + `  is not valid`
            }
        }
        return msg

    },
});
