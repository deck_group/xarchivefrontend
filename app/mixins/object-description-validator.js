import Mixin from '@ember/object/mixin';

export default Mixin.create({

    validate: function (value, label, max, min) {
        var msgArray = []
        msgArray.push(this.isValidLen(value, max, label))
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },

    isValidLen(value, maxthreshold, label) {
        var msg = ''
        if (value !== undefined) {
            if (value.length > maxthreshold) {
                msg = label + `  highest length ` + maxthreshold.toString()

            }
        }
        return msg
    }
});
