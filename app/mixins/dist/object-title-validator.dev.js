"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create({
  validate: function validate(value, isPresence, label, regexformat, checkleadingSpace, minLength, maxLength) {
    var msgArray = [];

    if (isPresence === true) {
      msgArray.push(this.presence(value, label));
    }

    if (checkleadingSpace === true) {
      msgArray.push(this.ifLeadingSpace(value, label));
    }

    msgArray.push(this.formatRule(value, regexformat, label));
    msgArray.push(this.checkingLength(value, minLength, maxLength, label));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  checkingLength: function checkingLength(value, minthreshold, maxthreshold, label) {
    var msg = '';

    if (value !== undefined) {
      if (value.length < minthreshold) {
        msg = label + " name atleast have " + minthreshold.toString();
      } else {
        if (value.length > maxthreshold) {
          msg = label + " name highest length " + maxthreshold.toString();
        }
      }
    }

    return msg;
  },
  formatRule: function formatRule(value, regexformat, label) {
    var msg = '';
    var patt = new RegExp(regexformat);

    if (patt.test(value) === false) {
      msg = label + " name format is not correct";
    }

    return msg;
  },
  ifLeadingSpace: function ifLeadingSpace(value, label) {
    var msg = '';

    if (value !== undefined) {
      var newtext = value.trim();

      if (newtext.length == 0) {
        msg = label + " name should not have only space";
      }
    }

    return msg;
  },
  presence: function presence(value, label) {
    var msg = '';

    if (value === undefined || value === "") {
      msg = label + " name is required";
    }

    return msg;
  }
});

exports["default"] = _default;