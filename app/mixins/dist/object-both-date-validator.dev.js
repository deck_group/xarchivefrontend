"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create({
  validatestart: function validatestart(value, isPresence, label, afterdatelabel, beforedatelabel) {
    var msgArray = [];

    if (isPresence === true) {
      msgArray.push(this.presence(value, label));
    }

    msgArray.push(this.invalid(value, label, beforedatelabel, afterdatelabel));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  validate: function validate(startdate, enddate, isPresence, label1, label2, afterdatelabel, beforedatelabel) {
    var msgArray = [];

    if (isPresence === true) {
      msgArray.push(this.presence(enddate, label2));
    }

    msgArray.push(this.invalid(enddate, label2, beforedatelabel, afterdatelabel));
    msgArray.push(this.correctDates(startdate, enddate, label1, label2));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  presence: function presence(value, label) {
    var msg = '';

    if (value === undefined || value === "") {
      msg = label + " is required";
    }

    return msg;
  },
  invalid: function invalid(value, label, before, after) {
    var msg = '';

    if (value !== "") {
      var check = new Date(value);
      before.setDate(before.getDate() + 1);
      var afterdt = new Date(after);

      if (check > afterdt && check < before) {} else {
        msg = label + "  is not valid";
      }
    }

    return msg;
  },
  correctDates: function correctDates(startdate, enddate, label1, label2) {
    var msg = '';
    var check1 = new Date(startdate);
    var check2 = new Date(enddate);

    if (check1 >= check2) {
      msg = "Check the " + label1 + " or " + label2;
    }

    return msg;
  }
});

exports["default"] = _default;