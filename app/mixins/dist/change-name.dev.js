"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

var _emberValidations = _interopRequireDefault(require("ember-validations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create(_emberValidations["default"], {
  validations: {
    "model.title": {
      presence: {
        message: 'mag niet leeg zijn'
      }
    }
  }
});

exports["default"] = _default;