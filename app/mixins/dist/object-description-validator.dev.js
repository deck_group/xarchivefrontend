"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create({
  validate: function validate(value, label, max, min) {
    var msgArray = [];
    msgArray.push(this.isValidLen(value, max, label));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  isValidLen: function isValidLen(value, maxthreshold, label) {
    var msg = '';

    if (value !== undefined) {
      if (value.length > maxthreshold) {
        msg = label + "  highest length " + maxthreshold.toString();
      }
    }

    return msg;
  }
});

exports["default"] = _default;