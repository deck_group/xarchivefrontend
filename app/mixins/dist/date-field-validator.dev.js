"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create({
  validate: function validate(value, isPresence, label, afterdatelabel, beforedatelabel) {
    var msgArray = [];

    if (isPresence === true) {
      msgArray.push(this.presence(value, label));
    }

    msgArray.push(this.invalid(value, label, beforedatelabel, afterdatelabel));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  presence: function presence(value, label) {
    var msg = '';

    if (value === undefined) {
      msg = label + " is required";
    }

    return msg;
  },
  invalid: function invalid(value, label, before, after) {
    var msg = '';

    if (value !== undefined) {
      var check = new Date(value);
      before.setDate(before.getDate() + 1);
      var afterdt = new Date(after);

      if (check > afterdt && check < before) {} else {
        msg = label + "  is not valid";
      }
    }

    return msg;
  }
});

exports["default"] = _default;