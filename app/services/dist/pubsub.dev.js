"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _evented = _interopRequireDefault(require("@ember/object/evented"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend(_evented["default"], {
  recogpublish: function recogpublish(content) {
    this.trigger('recognize', content);
  },
  dataPublish: function dataPublish(content, id) {
    this.trigger('newdata', content, id);
  },
  revisedPublish: function revisedPublish(content) {
    this.trigger('revisedrecognize', content);
  },
  markedPublish: function markedPublish(content) {
    this.trigger('markedrecognize', content);
  },
  assetPublish: function assetPublish(content) {
    this.trigger('asset', content);
  },
  sendDataPublish: function sendDataPublish(content) {
    this.trigger('recogA', content);
  },
  sendImageDataPublish: function sendImageDataPublish(content) {
    this.trigger('imagedatanew', content);
  },
  sendImageParameter: function sendImageParameter(content, flag) {
    this.trigger('imageparameter', content, flag);
  },
  sendAspectRatio: function sendAspectRatio(content) {
    this.trigger('aspectratio', content);
  },
  sendImageCorrectionOption: function sendImageCorrectionOption(content, holderflag) {
    this.trigger('imageCorrectionOption', content, holderflag);
  }
});

exports["default"] = _default;