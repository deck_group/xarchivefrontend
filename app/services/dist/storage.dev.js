"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend({
  imagestorage: "",
  width: 0,
  height: 0,
  isInverted: false,
  brigthness: 0,
  contrast: 0,
  hue: 0,
  saturation: 100,
  lighness: 50,
  parameter: undefined,
  route: undefined
});

exports["default"] = _default;