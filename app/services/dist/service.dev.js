"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend({
  signout: 'http://localhost:4200',
  imageurl: 'http://localhost:8080',
  url: 'http://localhost:8000/api/v1'
});

exports["default"] = _default;