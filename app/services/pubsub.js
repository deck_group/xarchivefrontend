import Service from '@ember/service';
import Evented from '@ember/object/evented';

export default Service.extend(Evented, {
    recogpublish: function (content) {
        this.trigger('recognize', content);
    },
    dataPublish: function (content, id) {
        this.trigger('newdata', content, id);
    },
    revisedPublish: function (content) {
        this.trigger('revisedrecognize', content);
    },
    markedPublish: function (content) {
        this.trigger('markedrecognize', content);
    },
    assetPublish: function (content) {
        this.trigger('asset', content);
    },
    sendDataPublish: function (content) {
        this.trigger('recogA', content);
    },
    sendImageDataPublish: function (content) {
        this.trigger('imagedatanew', content);
    },
    sendImageParameter: function (content, flag) {
        this.trigger('imageparameter', content, flag);
    },

    sendAspectRatio: function (content) {
        this.trigger('aspectratio', content);
    },
    sendImageCorrectionOption: function (content, holderflag) {
        this.trigger('imageCorrectionOption', content, holderflag);
    },
    selectedDirectoryPub: function (content) {
        this.trigger('selectedDirectory', content);
    },
    directoryvaluepub: function (content) {
        this.trigger('directoryvaluepub', content);
    },





});
