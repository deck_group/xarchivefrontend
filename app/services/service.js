import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
    signout: config.SIGNOUT_HOST,
    imageurl: config.IMAGE_HOST,
    url: config.URL_HOST,
});
