import Service from '@ember/service';

export default Service.extend({
    imagestorage:``,
    width:0,
    height:0,
    isInverted:false,
    brigthness:0,
    contrast:0,
    hue:0,
    saturation:100,
    lighness:50,
    parameter:undefined,
    route:undefined,

});
