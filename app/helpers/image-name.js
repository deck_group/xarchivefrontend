import { helper } from '@ember/component/helper';

export function imageName(params/*, hash*/) {
  return params[0]+'.jpg';
}

export default helper(imageName);
