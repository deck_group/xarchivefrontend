import { helper } from '@ember/component/helper';

export function currentRankId(params/*, hash*/) {
  return params[0] +params[1].toString();
}

export default helper(currentRankId);
