import { helper } from '@ember/component/helper';

export function imageComponent(params/*, hash*/) {
  return params[0][0].url;
}

export default helper(imageComponent);
