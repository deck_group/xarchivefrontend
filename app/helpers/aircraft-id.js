import { helper } from '@ember/component/helper';

export function aircraftId(params/*, hash*/) {
  var model = params[0]
  var airid = ''
  if (model.length === 0) {
    airid = 'AIR-000001'
  } else {

    var fakeid = model[0].aircraftid.split('-')[1]
    var newid = parseInt(fakeid) + 1
    airid = 'AIR-00000' + newid.toString()
  }
  return airid;
}

export default helper(aircraftId);
