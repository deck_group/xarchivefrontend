import { helper } from '@ember/component/helper';

export function noViewAccess(params/*, hash*/) {
  var tagname = 'unhide'
  params[0].forEach(function (data) {
    if(data.attribute===params[1]){
      tagname='hidden'
    }
  })
  return tagname;
}


export default helper(noViewAccess);
