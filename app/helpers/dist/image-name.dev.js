"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.imageName = imageName;
exports["default"] = void 0;

var _helper = require("@ember/component/helper");

function imageName(params
/*, hash*/
) {
  var model = params[0];
  var airid = '';

  if (model.length === 0) {
    airid = 'IMG-0000000001';
  } else {
    var fakeid = model[0].assetid.split('-')[1];
    var newid = parseInt(fakeid) + 1;
    airid = 'IMG-000000000' + newid.toString();
  }

  return airid + '.jpg';
}

var _default = (0, _helper.helper)(imageName);

exports["default"] = _default;