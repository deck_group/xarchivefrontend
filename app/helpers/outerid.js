import { helper } from '@ember/component/helper';

export function outerid(params/*, hash*/) {
  return params[0]+"_"+params[1];
}

export default helper(outerid);
