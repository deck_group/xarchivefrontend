import { helper } from '@ember/component/helper';

export function addOne(params/*, hash*/) {
  if (params[1].length > 0) {
    return params[0] + params[1].length;

  } else {
    return params[0]+1;

  }


}

export default helper(addOne);
