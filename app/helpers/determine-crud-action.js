import { helper } from '@ember/component/helper';

export function determineCrudAction(params/*, hash*/) {
  var tagname = 'none'
  params[0].forEach(function (data) {
    if (data.status === params[1]) {
      if (data.crudaction === params[2]) {
        tagname = data.crudaction
      }

    }
  })
  return tagname;
}


export default helper(determineCrudAction);
