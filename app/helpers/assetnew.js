import { helper } from '@ember/component/helper';

export function assetnew(params/*, hash*/) {
  var model = params[0]
  var airid = ''
  if (model.length === 0) {
    airid = 'IMG-0000000001'
  } else {
    var fakeid = model[0].assetid.split('-')[1]
    var newid = parseInt(fakeid) + 1
    airid = 'IMG-000000000' + newid.toString()
  }
  return airid;
}

export default helper(assetnew);
