import { helper } from '@ember/component/helper';

export function noEditAccess(params/*, hash*/) {
  var tagname = false
  params[0].forEach(function (data) {
    if(data.attribute===params[1]){
      tagname=true
    }
  })
  return tagname;
}

export default helper(noEditAccess);
