import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('login', {
    path: '/'
  });
  this.route('personnels');
  this.route('personnel-new', {
    path: '/personnel/new'
  });
  this.route('personnel-view', {
    path: '/personnel/:personnel_id/show'
  });
  this.route('personnel-edit', {
    path: '/personnel/:personnel_id/edit'
  });
  this.route('asset');
  this.route('asset-new', {
    path: '/asset/new'
  });
  this.route('aircrafts');
  this.route('aircraft-new', {
    path: '/aircraft/new'
  });
  this.route('aircraft-edit', {
    path: '/aircraft/:aircraft_id/edit'
  });
  this.route('aircraft-show', {
    path: '/aircraft/:aircraft_id/show'
  });
  this.route('asset-view', {
    path: '/asset/:asset_id/show'
  });
  this.route('asset-edit', {
    path: '/asset/:asset_id/edit'
  });
  this.route('edit-image');
  this.route('imageeditor');
  this.route('gallery');
  this.route('search');
  this.route('searchlist', {
    path: '/result/'
  });
  this.route('advance', {
    path: '/search/advance'
  });
  this.route('bulkupload',{path:'/bulk/upload'});
  this.route('assetparam',{path:'/:filter'});
  this.route('advancesearchresult');
});

export default Router;
