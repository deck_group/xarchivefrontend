'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'frontend',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.API_HOST = 'http://localhost:8000'
    ENV.IMAGE_HOST = 'http://127.0.0.1:8080'
    ENV.URL_HOST = 'http://localhost:8000/api/v1'
    ENV.SIGNOUT_HOST = 'http://localhost:4200'

  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    ENV.API_HOST = 'https://xarchivernewserver-dj24kv58-0d7nrx1y-dev.eu-west-1.klovercloud.com'
    ENV.IMAGE_HOST = 'https://xarchivephoto-e6rsko3n-ev0kgoki-dev.eu-west-1.klovercloud.com'
    ENV.URL_HOST = 'https://xarchivernewserver-dj24kv58-0d7nrx1y-dev.eu-west-1.klovercloud.com/api/v1'
    ENV.SIGNOUT_HOST = ''
  }

  return ENV;
};
